#!/usr/bin/gnuplot
set term x11 persist
set grid
set style data lines


filenames = "`ls -C *.dat`"

plot for[file in filenames] file title file
