#-------------------------------------------------
#
# Project created by QtCreator 2015-05-26T08:30:03
#
#-------------------------------------------------

QT       -= core
QT       -= gui

CONFIG  += console
CONFIG  += opengl
CONFIG  += c++11
CONFIG  -= app_bundle

DESTDIR = $$PWD/bin

TEMPLATE = app

OBJECTS_DIR = ./obj

QMAKE_CXXFLAGS += -pedantic -Wall

CONFIG(debug, debug|release) {
	DEFINES = DEBUG
}
CONFIG(release, debug|release) {
	DEFINES = RELEASE
}

SOURCES += \
    src/gui/Button.cpp \
    src/gui/Checkbox.cpp \
    src/gui/Container.cpp \
    src/gui/Control.cpp \
    src/gui/GUI.cpp \
    src/gui/Label.cpp \
    src/gui/Radio.cpp \
    src/gui/Slider.cpp \
    src/world/Mind.cpp \
    src/world/Organism.cpp \
    src/world/Sensor.cpp \
    src/world/SpeciesManager.cpp \
    src/world/World.cpp \
    src/AudioManager.cpp \
    src/ColourManager.cpp \
    src/FontManager.cpp \
    src/main.cpp \
    src/PlayState.cpp \
    src/Signal.cpp \
    src/StartState.cpp \
    src/TextureManager.cpp \
    src/Util.cpp \
    src/WindowManager.cpp \
    src/world/Steering.cpp \
    src/world/EntityManager.cpp \
    src/Animation.cpp \
    src/world/Map.cpp

HEADERS += \
    src/gui/Button.h \
    src/gui/Checkbox.h \
    src/gui/Container.h \
    src/gui/Control.h \
    src/gui/Desktop.h \
    src/gui/GUI.h \
    src/gui/Label.h \
    src/gui/Radio.h \
    src/gui/Slider.h \
    src/gui/Tools.h \
    src/gui/Window.h \
    src/world/BoxEntity.h \
    src/world/Entity.h \
    src/world/FoodSource.h \
    src/world/MentalState.h \
    src/world/Mind.h \
    src/world/Organism.h \
    src/world/Sensor.h \
    src/world/Species.h \
    src/world/SpeciesManager.h \
    src/world/World.h \
    src/AudioManager.h \
    src/ColourManager.h \
    src/FontManager.h \
    src/PlayState.h \
    src/Signal.h \
    src/StartState.h \
    src/TextureManager.h \
    src/Util.h \
    src/WindowManager.h \
    src/world/Steering.h \
    src/world/EntityManager.h \
    src/world/Plant.h \
    src/world/Remains.h \
    src/Animation.h \
    src/world/Map.h

INCLUDEPATH += $$PWD/lib/CGF/include \
               $$PWD/lib/CGF/include/tmxloader \
               $$PWD/lib/CGF/include/pugixml \
               $$PWD/lib/Box2D-2.3.0/include \
               $$PWD/lib/SFML-2.2/include

DEPENDPATH += $$PWD/lib/CGF/include \
              $$PWD/lib/CGF/include/tmxloader \
              $$PWD/lib/CGF/include/pugixml \
              $$PWD/lib/Box2D-2.3.0/include \
              $$PWD/lib/SFML-2.2/include

LIBS += -L$$PWD/lib

macx {

	TARGET = Terra-OSX

    LIBS += -L$$PWD/lib/Box2D-2.3.0/lib-OSX \
			-L$$PWD/lib/SFML-2.2/lib-OSX \
            -lsfml-graphics \
            -lsfml-window \
            -lsfml-system \
            -lsfml-audio \
            -lCGF-OSX \
            -lBox2D \
            -lz

	QMAKE_RPATHDIR += $$PWD/lib/SFML-2.2/lib-OSX

    PRE_TARGETDEPS += $$PWD/lib/libCGF-OSX.a

}

unix:!macx {

	TARGET = Terra-Linux

    LIBS += -L$$PWD/lib/Box2D-2.3.0/lib-Linux64 \
			-L$$PWD/lib/SFML-2.2/lib-Linux64 \
            -lsfml-graphics \
            -lsfml-window \
            -lsfml-system \
            -lsfml-audio \
            -lCGF-Linux \
            -lBox2D \
            -lz

	QMAKE_RPATHDIR += $$PWD/lib/SFML-2.2/lib-Linux64

    PRE_TARGETDEPS += $$PWD/lib/libCGF-Linux.a

}

