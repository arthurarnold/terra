//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef MAP_H
#define MAP_H

#include "SFML/Graphics.hpp"
#include "../Util.h"


class Map : public sf::Drawable
{
public:

    struct Tile
    {
        unsigned long emptyTicks;
        float fertility;
        bool water;
        bool occupied;

        Tile() : emptyTicks(1),water(false),occupied(false)
        {
            fertility = util::randf();
        }
    };

    Map() {}
    ~Map();

    // Create the map with w*h pixels
    void create(unsigned tilesX, unsigned tilesY, unsigned tileS);

    Tile & at(unsigned x, unsigned y);
    Tile & at(const sf::Vector2f & idx);
    Tile & atPixel(float x, float y);

    void draw(sf::RenderTarget & target, sf::RenderStates states) const override;
    void update();

    const sf::Vector2f & getMainSpawnPoint();

private:

    sf::Texture m_texture;
    sf::Sprite m_sprite;

    sf::FloatRect rect;

    Tile * m_tiles;
    sf::Vector2u m_dim;
    float m_tileSize;

    unsigned char * m_waterPixels;
    unsigned char * m_landPixels;

    sf::Vector2f m_mainSpawnPoint;

    void setTile(unsigned i, unsigned j, bool water);
    void initialiseColours();
    void initSpecies();
    void freeTileAtPixel(const sf::Vector2f & p);
    void makeSpawnArea(const sf::Vector2f & p);
    void addPlant(int x, int y);
};

#endif // MAP_H

