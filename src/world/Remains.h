//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef REMAINS_H
#define REMAINS_H

#include "Species.h"
#include "FoodSource.h"

class Remains : public FoodSource
{
    friend class Organism;
public:
    Remains(Species * species, float amount, float radius) :
        FoodSource(species),
        m_amount(amount),
        m_startingAmount(amount)
    {
        m_body.setRadius(radius);
        m_body.setFillColor(sf::Color::Transparent);
        m_body.setOutlineColor(species->colour);
        m_body.setOutlineThickness(-1);
        m_body.setOrigin(radius,radius);
    }

    ////////// FoodSource //////////////////////////////////////////////////////

    float eat()
    {
        static sf::Color modulation(255,255,255,255);
        m_amount -= 0.2;

        if(m_amount < 0)
            m_amount = 0;

        modulation.a = 255 * m_amount / m_startingAmount;
        m_body.setOutlineColor(m_species->colour * modulation);
        return 0.2f;
    }

    ////////// Entity //////////////////////////////////////////////////////////

    void setPosition(const sf::Vector2f & pos) override
    {
        m_position = pos;
        m_body.setPosition(pos);
    }

    float getDistance(const sf::Vector2f & p) const override
    {
        return util::length(m_position-p) - m_body.getRadius();
    }

    sf::Vector2f getDirectionTo(const sf::Vector2f & p) const override
    {
        return p - m_position;
    }

    void draw(sf::RenderTarget & target, sf::RenderStates state) const override
    {
        target.draw(m_body, state);
    }

    bool update() override
    {
        return m_amount > 0;
    }

private:
    sf::CircleShape m_body;
    float m_amount;
    float m_startingAmount;
};

#endif // REMAINS_H

