//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Sensor.h"

#include <iostream>

#include "../Util.h"
#include "Organism.h"
#include "EntityManager.h"

using namespace std;

void Sensor::sense()
{
    float d;

    clearData();

    const EntityMgr::Set & entities = EntityMgr::getAll();
    const sf::Vector2f & ppos = m_parent->getPosition();
    Species * sp = m_parent->getSpecies();

    Organism * other = NULL;

    for(Entity * e : entities)
    {
        if(e == m_parent)
            continue; // skip the entity itself;

        d = e->getDistance(ppos);
        if(d <= m_range)
        {
            m_data.push_back(e);

            if(d < m_distToClosest)
            {
                m_distToClosest = d;
                m_closest = e;
            }

            if(d <= sp->cruiseDistance())
                m_collidables.push_back(e);


            switch(e->getType())
            {
			case FOOD_SOURCE:
                m_foodSources.push_back(e);
				if(d < m_distToClosestFoodSource)
                {
                    m_distToClosestFoodSource = d;
                    m_closestFoodSource = static_cast<FoodSource *>(e);
                }
                break;
            case ORGANISM:
				other = static_cast<Organism *>(e);
				// Check if predator
                if(other->getSpecies()->preysUpon(sp))
				{
					// other eats this
                    m_predators.push_back(e);
					if(d < m_distToClosestPredator)
					{
						m_distToClosestPredator = d;
						m_closestPredator = other;
					}
				}
                if(sp->preysUpon(other->getSpecies()))
				{
					// this eats other
                    m_prey.push_back(e);
					if(d < m_distToClosestPrey)
					{
						m_distToClosestPrey = d;
						m_closestPrey = other;
					}
				}
				break;
			case OBSTACLE:
				break;
			default:
				break;
            }
        }
    }
}

void Sensor::clearData()
{
    m_data.clear();
    m_collidables.clear();
    m_predators.clear();
    m_prey.clear();
    m_foodSources.clear();

    m_closest = NULL;
    m_distToClosest = m_range;

    m_closestFoodSource = NULL;
    m_distToClosestFoodSource = m_range;

    m_closestPredator = NULL;
    m_distToClosestPredator = m_range;

    m_closestPrey = NULL;
    m_distToClosestPrey = m_range;

}
