//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef PLANT_H
#define PLANT_H

#include "FoodSource.h"
#include "Species.h"

class Plant : public FoodSource
{
public:

    Signal<const sf::Vector2f &> OnDie;

    Plant(Species * species) :
        FoodSource(species),
        m_foodAmount(8.0f)

    {
        assert(species->type == Species::PLANT);
        float r = m_foodAmount/2.0f;
        m_body.setPointCount(3);
        m_body.setRadius(r);
        m_body.setFillColor(species->colour);
        m_body.setOrigin(r,r);
        species->incrementPopulation();
    }

    ~Plant()
    {
        OnDie(getPosition());
        m_species->decrementPopulation();
    }

    float eat() override
    {
    	--m_foodAmount;
        float r = m_foodAmount/2.0f;
        m_body.setRadius(r);
        m_body.setOrigin(r,r);

        return 1.0f;
    }

/*    float getFoodAmount() override
    {
        return m_body.getRadius();
    }
*/

    bool update() override
    {
        return m_foodAmount > 0;
    }

    void setPosition(const sf::Vector2f & pos) override
    {
        m_position = pos;
        m_body.setPosition(pos);
    }

    float getDistance(const sf::Vector2f & p) const override
    {
        return util::length(p - m_position) - m_body.getRadius();
    }

    sf::Vector2f getDirectionTo(const sf::Vector2f & p) const override
    {
        return p - m_position;
    }

    void draw(sf::RenderTarget & target, sf::RenderStates state) const
    {
        target.draw(m_body, state);
    }

private:
    sf::CircleShape m_body;
    float m_foodAmount;
};

#endif // PLANT

