//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Steering.h"

#include "../Util.h"
#include "Entity.h"

#include <iostream>
using namespace std;

namespace steering
{
    ////////// INTERNAL //////////////////////////

        const float WANDER_CIRCLE_DELTA = 50.0f;
        const float WANDER_CIRCLE_RADIUS = 10.0f;
        const float WANDER_ANGLE_CHANGE = 0.5f;

    //////////////////////////////////////////////

    sf::Vector2f avoid(const Entity * subject, const Entity * target)
    {
        return util::norm(target->getDirectionTo(subject->getPosition()));
    }

    //////////////////////////////////////////////

    sf::Vector2f flee(const Entity * subject, const Entity * target)
    {
        static const float lookAhead = 5.0f;

        return util::norm(target->getDirectionTo(subject->getPosition())-lookAhead*target->getVelocity());
    }

    //////////////////////////////////////////////

    sf::Vector2f seek(const Entity * subject, const Entity * target, float stopDistance, float maxAcc)
    {
        float s = util::length(subject->getVelocity());

        // By Torricelli's equation, calculate the
        // distance required to stop the subject when
        // applying maximum acceleration.
        float ad = (s*s) / (2.0f*maxAcc);

        if(target->getDistance(subject->getPosition()) - stopDistance <= ad)
            return sf::Vector2f(0.0f,0.0f);

        return -util::norm(target->getDirectionTo(subject->getPosition()));
    }

    //////////////////////////////////////////////

    sf::Vector2f pursue(const Entity * subject, const Entity * target, float stopDistance, float maxAcc)
    {
        static const float lookahead = 5.0f;
        return util::norm(-(target->getDirectionTo(subject->getPosition()))+lookahead*target->getVelocity());
    }

    //////////////////////////////////////////////

    sf::Vector2f wander(const Entity * subject)
    {
        // Circle centre is in entity's velocity direction.
        sf::Vector2f cc = util::norm(subject->getVelocity());
        sf::Vector2f delta = cc * WANDER_CIRCLE_RADIUS;

        // Translate circle centre to WANDER_CIRCLE_DELTA
        // units in front of entity.
        cc *= WANDER_CIRCLE_DELTA;

        // Rotate delta by a random angle.
        float angle = util::randf(-WANDER_ANGLE_CHANGE,WANDER_ANGLE_CHANGE);
        util::rotate(delta, angle);

        // Translate delta to circle centre.
        delta += cc;

        return util::norm(delta);
    }
}
