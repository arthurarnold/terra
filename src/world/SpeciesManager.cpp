//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "SpeciesManager.h"
#include <pugixml.hpp>
#include <iostream>

#define FILL(PROP,TYPE) \
    if((attr = species_node.child(#PROP))) \
        { species->PROP = attr.text().as_##TYPE(); }

using namespace std;

namespace SpeciesMgr
{
	SpeciesMap m_species;
	int secCount = 0;

    void fetchPreys(Species *, const pugi::xml_node &);

	void init()
    {
		pugi::xml_document doc;
		if(!doc.load_file("data/config/species.xml"))
		{
			cout << "Error loading species.xml" << endl;
			return;
		}

        Species * species = NULL;
        pugi::xml_node attr;

        for( pugi::xml_node species_node : doc.child("xml").children())
		{
			species = new Species();

			if(!species_node.attribute("id").empty())
				species->id = species_node.attribute("id").value();
			else
			{
				cout << "[SpeciesMgr] ERROR: unidentified species in config/species.xml" << endl;
				std::abort();
			}

			if(!species_node.attribute("type").empty())
			{
			    std::string typestr(species_node.attribute("type").value());
			    if(typestr == "animal")
                    species->type = Species::ANIMAL;
                else if(typestr == "plant")
                    species->type = Species::PLANT;
                else
                    cout << "[SpeciesMgr] WARNING: Unknown species type '"
                         << typestr << "'" << endl;
			}


            FILL(minLifespan,int)
            FILL(maxLifespan,int)

			if((attr = species_node.child("colour")))
			{
				pugi::xml_node subattr;
				if((subattr = attr.child("r")))
					species->colour.r = subattr.text().as_int();
				if((subattr = attr.child("g")))
					species->colour.g = subattr.text().as_int();
				if((subattr = attr.child("b")))
					species->colour.b = subattr.text().as_int();
			}

            FILL(radius, float)
            FILL(sensorRadius, float)
            FILL(stomachHold, float)
            FILL(digestionRate, float)
            FILL(aggressive, bool)
            FILL(attackDamage, float)
            FILL(reproductionInterval, int)

            FILL(gamma, float)
            FILL(sigma, float)
            FILL(alpha, float)
            FILL(epsilon_e, float)
            FILL(epsilon_a, float)
            FILL(tau, float)
            FILL(rho_m, float)
            FILL(rho_M, float)

			fetchPreys(species,species_node.child("preys"));

            m_species[species->id] = species;
		}

		cout << "Species map initialised. " << m_species.size() << " species added." << endl;

		if(m_species.find("main") == m_species.end())
		{
			cout << "[SpeciesMgr] ERROR: no 'main' species defined." << endl;
			std::abort();
		}
	}

	void fetchPreys(Species * s, const pugi::xml_node & n)
	{
		for(pugi::xml_node & p : n.children("prey"))
        {
			s->addPrey(m_species[p.child_value()]);
		}
	}

	void cleanup()
	{
		for(auto & p : m_species)
			delete p.second;
	}

	const SpeciesMap & getAll()
	{
		return m_species;
	}

    size_t count()
    {
        return m_species.size();
    }

    size_t animalCount()
    {
        return std::count_if(m_species.begin(),
                             m_species.end(),
                             [](SpeciesMap::value_type & p) {
                                return p.second->type == Species::ANIMAL;
                            });
    }

	Species * get(const std::string & id)
	{
		SpeciesMap::iterator it = m_species.find(id);

		if(it != m_species.end())
			return it->second;

		cout << "[SpeciesMgr] ERROR: no species '" << id << "' found." << endl;
		return NULL;
	}
}
