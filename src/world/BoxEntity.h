//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef BOXENTITY_H
#define BOXENTITY_H

#include "Entity.h"

class BoxEntity : public Entity
{
public:
    BoxEntity() : Entity(OBSTACLE) {}

    void setBox(const sf::FloatRect & box) { m_box = box; }

    float getDistance(const sf::Vector2f & p) const override
    {
        sf::Vector2f pToWall = projectToClosestWall(p);

        return abs((int)(pToWall.x == 0 ? pToWall.y : pToWall.x));
    }

    sf::Vector2f getDirectionTo(const sf::Vector2f & p) const override
    {
        return -projectToClosestWall(p);
    }

private:
    sf::FloatRect m_box;

    sf::Vector2f projectToClosestWall(const sf::Vector2f & p) const
    {
        // Left wall
        float d = p.x - m_box.left;
        sf::Vector2f ret(-d, 0.0f);

        // Right wall
        float aux = m_box.left + m_box.width - p.x;
        if(aux < d)
        {
            d = aux;
            ret = sf::Vector2f(d, 0.0f);
        }

        // Top wall
        aux = p.y - m_box.top;
        if(aux < d)
        {
            d = aux;
            ret = sf::Vector2f(0.0f, -d);
        }

        // Bottom wall
        aux = m_box.top + m_box.height - p.y;
        if(aux < d)
        {
            d = aux;
            ret = sf::Vector2f(0.0f, d);
        }

        return ret;
    }
};

#endif // BOXENTITY_H
