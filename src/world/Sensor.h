//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef SENSOR_H
#define SENSOR_H

#include <list>

#include "World.h"
#include "Entity.h"
#include "FoodSource.h"

class Organism;

class Sensor
{
public:
    Sensor(Organism * parent) : m_parent(parent) {}

    // Updates the information
    void sense();

    void setRange(float r) { m_range = r; }
    float getRange() const { return m_range; }

    Entity * getClosest() const { return m_closest; }
    float getDistanceToClosest() const { return m_distToClosest; }

    FoodSource * getClosestFoodSource() const { return m_closestFoodSource; }
    float getDistanceToClosestFoodSource() const { return m_distToClosestFoodSource; }

    Organism * getClosestPredator() const { return m_closestPredator; }
    float getDistanceToClosestPredator() const { return m_distToClosestPredator; }

    Organism * getClosestPrey() const { return m_closestPrey; }
    float getDistanceToClosestPrey() const { return m_distToClosestPrey; }

//    const EntityList & data() { return m_data; }

    const EntityList & getAll() { return m_data; }
    const EntityList & getCollidables() { return m_collidables; }
    const EntityList & getPredators() { return m_predators; }
    const EntityList & getPrey() { return m_prey; }
    const EntityList & getFoodSources() { return m_foodSources; }

private:
    Organism * m_parent;

    float m_range;

    // The data collected when sense() was called.
    EntityList m_data;
    EntityList m_collidables;
    EntityList m_predators;
    EntityList m_prey;
    EntityList m_foodSources;

    Entity * m_closest;
    float m_distToClosest;

    FoodSource * m_closestFoodSource;
    float m_distToClosestFoodSource;

    Organism * m_closestPredator;
    float m_distToClosestPredator;

    Organism * m_closestPrey;
    float m_distToClosestPrey;

    void clearData();
};

#endif // SENSOR_H
