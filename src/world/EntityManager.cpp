//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "EntityManager.h"

#include "Organism.h"
#include "Entity.h"
#include "World.h"

namespace EntityMgr
{
    Set entities;

    void add(Entity * entity)
    {
        entities.insert(entity);
    }

    void remove(Entity * entity)
    {
        entities.erase(entity);
    }

    void cleanup()
    {
        for(Entity * e : entities)
            delete e;
    }

    const Set & getAll()
    {
        return entities;
    }

    int count()
    {
        return entities.size();
    }

    void draw(sf::RenderWindow & window)
    {
        for(Entity * e : entities)
            window.draw(*e);
    }

    void update()
    {
        for(Set::iterator it = entities.begin(); it != entities.end();)
        {
            if(!(*it)->update())
            {
                delete (*it);
                it = entities.erase(it);
            }
            else
                ++it;
        }
    }

    bool exists(Entity * entity)
    {
        if(entity == NULL)
            return false;

        return entities.find(entity) != entities.end();
    }

    void select(const sf::Vector2f & point)
    {
        static Organism * selected = NULL;
        Organism * o;
        for(Entity * e : entities)
        {
            if(e->getType() == ORGANISM)
            {
                o = static_cast<Organism *>(e);
                if(o->contains(point))
                {
                    if(exists(selected))
                        selected->deselect();
                    o->select();
                    selected = o;
                    return;
                }
            }
        }

        if(exists(selected))
        {
            selected->deselect();
            selected = NULL;
        }
    }
}
