//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef FOODSOURCE_H
#define FOODSOURCE_H

#include "Game.h"
#include "../Util.h"
#include "Entity.h"

class FoodSource : public Entity
{
public:
    FoodSource(Species * species) :
        Entity(FOOD_SOURCE),
        m_species(species)
    {
    }

    virtual ~FoodSource() {}

    virtual float eat() = 0;

    //virtual float getFoodAmount() = 0;

protected:
    Species * m_species;
};

#endif // FOODSOURCE_H
