//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef ORGANISM_H
#define ORGANISM_H

#include <random>
#include <iostream>

#include "../Util.h"
#include "Entity.h"
#include "Species.h"

class Sensor;
class Mind;

#define ANGLE_CHANGE 2.0f

#define WANDER_CIRCLE_DISTANCE 50.0f
#define WANDER_CIRCLE_RADIUS 20.0f

using namespace std;

class Organism : public Entity
{
public:

    // Give Mind access to Organism's
    // non-public members.
	friend class Mind;

	////////////////////////////////////////////////////////////////////////////

	struct Action
	{
		enum Type
		{
            MOVE,
			EAT,			// Eat target (FoodSource or dead Organism)
            FIGHT,			// Fight target (living organism)
            REPRODUCE
		};

        Type type; // This action's type

        sf::Vector2f acc; // Acceleration

        // This actions target.
        // (for non-MOVE actions only)
        Entity * target;


	};

	////////////////////////////////////////////////////////////////////////////

    Organism(Species * species);

    ~Organism();

    // Sets the position of the organism's centre
    void setPosition(const sf::Vector2f & pos);

    void setAge(int age);
    void setFoodLevel(int foodLevel);

    void setVelocity(const sf::Vector2f & v);

    Species * getSpecies();

    ///// Inherited from Entity /////

        float getDistance(const sf::Vector2f & p) const override;

        sf::Vector2f getDirectionTo(const sf::Vector2f & p) const override;

        void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

        bool update() override;

        const sf::Vector2f & getVelocity() const override { return m_velocity; }

    /////////////////////////////////

    void decreaseHealth(float decrement);

    /////////////////////////////////

    bool contains(const sf::Vector2f & point);
    void select();
    void deselect();

private:
	Species * m_species;

	// The organism's current action.
	Action m_action;

    // How many update ticks this organism can live.
    // [m_species->minLifespan, m_species->maxLifespan]
    int m_lifespan;

    // [0,m_lifespan]
    // How many update ticks this organism has lived.
    int m_age = 0;

    float m_health = 1.0f;

    int m_timeSinceLastReproduction = 0;
    int m_timeSinceLastPredatorSeen = 0;

    /////////// Behaviour manipulation ///////////

		// The organism's sensor object, which perceives
		// the environment around within a finite range.
		Sensor * m_sensor;

		Mind * m_mind;

    /////////// Physiologic states ///////////////

		float m_foodLevel;

	//////////////////////////////////////////////

    // The organism's current velocity.
    sf::Vector2f m_velocity;

    // The organism's body to be displayed.
    sf::CircleShape m_body;

    bool m_selected = false;

    sf::CircleShape m_visionCircle;
    sf::Text m_infoText;

    // PRIVATE METHODS ////////////////////////////////////////////////

    ///// Steering behaviours /////

        // Produces an acceleration vector and updates
        // the organism's velocity and position according
        // to the current intention chosen by the intention
        // generator.
        void steer();

        void eat(Entity * target);

        void fight(Entity * target);

        void reproduce();

    ///////////////////////////////

    bool die();

    void setBodyRadius(float r);

    void updateInfoText();

};


#endif // ORGANISM_H
