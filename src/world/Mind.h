//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef MIND_H
#define MIND_H

class Sensor;
class Entity;
#include "Organism.h"

class Mind
{
public:
	Mind(Organism * parent);
	~Mind();

	void update();

protected:
private:

	Organism * m_parent;

    bool avoid_vel(sf::Vector2f & dv);
    bool flee_vel(sf::Vector2f & dv);
    bool eat_vel(sf::Vector2f & dv);
    bool prevent_vel(sf::Vector2f & dv);
    void wander_vel(sf::Vector2f & dv);

    bool reproductionDesired();

};


#endif // MIND_H

