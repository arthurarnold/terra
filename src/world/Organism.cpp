//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Organism.h"

#include "Sensor.h"
#include "Mind.h"
#include "../Util.h"
#include "../FontManager.h"
#include "FoodSource.h"
#include "Steering.h"
#include "EntityManager.h"
#include "Remains.h"
#include "../Animation.h"


Organism::Organism(Species * species) :
	Entity(ORGANISM),
	m_species(species),
    m_lifespan(util::randi(species->minLifespan, species->maxLifespan)),
    m_sensor(new Sensor(this)),
    m_mind(new Mind(this)),
    m_foodLevel(species->eatTrigger()/2.0f)
{
//////////////////////////////////////////////////
//	Uncomment these two lines to have main organisms
//	die quickly so that the GameOverState can be tested.
//	if(species->id == "main")
//		m_lifespan = 360;
//////////////////////////////////////////////////

    assert(m_species->type == Species::ANIMAL);
    m_species->incrementPopulation();

    // Body properties
    m_body.setFillColor(sf::Color::Transparent);
    m_body.setPointCount(m_species->radius*3);
    m_body.setOutlineColor(m_species->colour);
    setBodyRadius(3.0f);

    // Components
    m_sensor->setRange(m_species->sensorRadius);

    m_visionCircle.setFillColor(sf::Color::Transparent);
    m_visionCircle.setOutlineThickness(1.0f);

    m_visionCircle.setRadius(m_species->sensorRadius);
    m_visionCircle.setOutlineColor(sf::Color(200,200,200));
    m_visionCircle.setOrigin(m_species->sensorRadius,m_species->sensorRadius);

    m_infoText.setFont(FontMgr::Label);
    m_infoText.setCharacterSize(14);
    m_infoText.setOrigin(sf::Vector2f(0.0f,-m_species->radius-5.0f));
    m_infoText.setColor(ColourMgr::Label);


    m_velocity.x = rand();
    m_velocity.y = rand();
    setVelocity(m_velocity);
}

Organism::~Organism()
{
	delete m_mind;
	delete m_sensor;
}

//////////////////////////////////////////////////

void Organism::setPosition(const sf::Vector2f & pos)
{
    m_position = pos;
    m_body.setPosition(pos);
    m_visionCircle.setPosition(pos);
    m_infoText.setPosition(pos);
}

//////////////////////////////////////////////////

void Organism::setAge(int age)
{
    m_age = age;
    setBodyRadius(util::map(age,0,m_lifespan,3.0f,m_species->radius));
}

//////////////////////////////////////////////////

void Organism::setFoodLevel(int foodLevel)
{
    m_foodLevel = foodLevel;
}

//////////////////////////////////////////////////

void Organism::setVelocity(const sf::Vector2f & v)
{
    m_velocity = v;
    util::truncate(m_velocity, m_species->maxSpeed());
}

//////////////////////////////////////////////////

Species * Organism::getSpecies()
{
	return m_species;
}

//////////////////////////////////////////////////

void Organism::decreaseHealth(float decrement)
{
    m_health -= decrement;
}

//////////////////////////////////////////////////

void Organism::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_body, state);

    if(m_selected)
    {
        target.draw(m_visionCircle, state);
        target.draw(m_infoText, state);
    }
}

//////////////////////////////////////////////////

bool Organism::update()
{
    static const float starveHealthDec = 0.005f;
    static const float deathAnimationDuration = 36.0f;

    m_foodLevel -= m_species->digestionRate;
    setAge(m_age+1);

    if(m_foodLevel <= 0)
    {
        m_health -= starveHealthDec;
        m_foodLevel = 0;
    }
    else if(m_health < 1.0f)
    {
        m_health += starveHealthDec;
    }

    if(m_health <= deathAnimationDuration*starveHealthDec)
        m_body.setOutlineThickness(util::map(m_health,
                                             deathAnimationDuration*starveHealthDec, 0.0f,
                                             -m_body.getRadius(), -1.0f));

    if(m_health <= 0 || m_age >= m_lifespan)
        return die();

    m_sensor->sense();

    ++m_timeSinceLastReproduction;

    if(m_sensor->getClosestPredator() == NULL)
        ++m_timeSinceLastPredatorSeen;
    else
        m_timeSinceLastPredatorSeen = 0;

    m_mind->update();

    if(m_selected)
        updateInfoText();

    steer();

    if(m_lifespan - m_age < deathAnimationDuration)
    {
        m_body.setOutlineThickness(util::map(m_lifespan-m_age,
                                             deathAnimationDuration, 0.0f,
                                             -m_body.getRadius(), -1.0f));
    }

    return true;
}

//////////////////////////////////////////////////

float Organism::getDistance(const sf::Vector2f & p) const
{
    return util::length(p - m_position) - m_body.getRadius();
}

//////////////////////////////////////////////////

sf::Vector2f Organism::getDirectionTo(const sf::Vector2f & p) const
{
    return p - (m_position);
}

void Organism::steer()
{
    switch(m_action.type)
    {
    case Action::MOVE:
        setVelocity(m_velocity + m_action.acc);
        setPosition(getPosition() + m_velocity);
        break;
    case Action::EAT:
        eat(m_action.target);
        break;
    case Action::FIGHT:
        fight(m_action.target);
        break;
    case Action::REPRODUCE:
        reproduce();
        break;
    }
}

void Organism::eat(Entity * target)
{
    if(target->getType() != FOOD_SOURCE)
		return;
    FoodSource * f = static_cast<FoodSource *>(target);
    m_foodLevel += f->eat();
}

//////////////////////////////////////////////////

void Organism::fight(Entity * target)
{
    static_cast<Organism *>(target)->decreaseHealth(m_species->attackDamage);
}

void Organism::reproduce()
{
    m_timeSinceLastReproduction = 0;
    m_age /= 2;
    m_foodLevel /= 2.0f;

    Organism * o = new Organism(m_species);
    o->setPosition(m_position);
    o->setAge(m_age);
    o->setFoodLevel(m_foodLevel);
    EntityMgr::add(o);

}

//////////////////////////////////////////////////

bool Organism::die()
{
    Remains * r = new Remains(m_species, m_foodLevel, m_body.getRadius());
    r->setPosition(m_position);
    EntityMgr::add(r);
    m_species->decrementPopulation();
    return false;
}

////////////////////////////////////////////////////////////////////////////////

bool Organism::contains(const sf::Vector2f & point)
{
    return util::sqdist(m_position,point) <= (m_body.getRadius()*m_body.getRadius());
}

void Organism::select()
{
    m_selected = true;
}

void Organism::deselect()
{
    m_selected = false;
}

void Organism::setBodyRadius(float r)
{
    m_body.setRadius(r);
    m_body.setOutlineThickness(-r);
    m_body.setOrigin(r,r);
}

void Organism::updateInfoText()
{
    char buf[64];
    int n = snprintf(buf,sizeof(buf),
                     "Food level: %.2f\nAge: %d / %d",
                     m_foodLevel, m_age, m_lifespan);
    if((unsigned long)n > sizeof(buf))
        cout << "[Organism::updateInfoText()] Buffer too small" << endl;
    m_infoText.setString(buf);
}
