//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef SPECIES_MANAGER_H
#define SPECIES_MANAGER_H

#include "Species.h"

namespace SpeciesMgr
{
	typedef std::map<std::string, Species *> SpeciesMap;

	void init();
	void cleanup();

	const SpeciesMap & getAll();
    size_t count();
    size_t animalCount();
    Species * get(const std::string & id);
}

#endif // SPECIES_MANAGER_H
