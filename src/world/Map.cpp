//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Map.h"

#include <iostream>
#include <cstring>
#include <cmath>
#ifndef M_PI
#   define M_PI 3.14159265359
#endif // M_PI

#include "../Util.h"
#include "SpeciesManager.h"
#include "EntityManager.h"
#include "Organism.h"
#include "Plant.h"
#include "BoxEntity.h"

const unsigned char WATER_COLOUR[] = {  67, 137, 254, 255 };
//const unsigned char LAND_COLOUR[]  = { 138, 194,  73, 255 };
const unsigned char LAND_COLOUR[]  = { 195, 233,  175, 255 };
//const unsigned char LAND_COLOUR[]  = { 255, 0,  0, 255 };

// Spawn area radius, in tiles.
const int SPAWN_AREA_RADIUS = 10;

// Ocean's threshold, in tiles.
const int OCEAN_THRESHOLD = 5;

////////////////////////////////////////////////////////////////////////////////

Map::~Map()
{
    delete [] m_landPixels;
    delete [] m_waterPixels;
    delete [] m_tiles;
}

////////////////////////////////////////////////////////////////////////////////

void Map::create(unsigned tilesX, unsigned tilesY, unsigned tileS)
{
    m_dim = sf::Vector2u(tilesX,tilesY);
    m_tileSize = tileS;

    m_tiles = new Tile[tilesX*tilesY];

    initialiseColours();


    m_texture.create(tilesX*tileS,tilesY*tileS);

    for(unsigned x=0; x<tilesX; ++x)
        for(unsigned y=0; y<OCEAN_THRESHOLD; ++y)
        {
            setTile(x,y,true);
            setTile(x,m_dim.y-y-1,true);
        }

    for(unsigned y=0; y<tilesY; ++y)
        for(unsigned x=0; x<OCEAN_THRESHOLD; ++x)
        {
            setTile(x,y,true);
            setTile(m_dim.x-x-1,y,true);
        }

    BoxEntity * b = new BoxEntity();

    rect.top = tileS*OCEAN_THRESHOLD;
    rect.left = tileS*OCEAN_THRESHOLD;
    rect.width = (tilesX-2*OCEAN_THRESHOLD)*tileS;
    rect.height = (tilesY-2*OCEAN_THRESHOLD)*tileS;
    b->setBox(rect);
    EntityMgr::add(b);

    // Place an organism of each species
    // on its own spawn point.
    initSpecies();

    m_sprite = sf::Sprite(m_texture);
}

////////////////////////////////////////////////////////////////////////////////

Map::Tile & Map::at(unsigned x, unsigned y)
{
    return m_tiles[y*m_dim.x + x];
}

////////////////////////////////////////////////////////////////////////////////

Map::Tile & Map::at(const sf::Vector2f & idx)
{
    return at(idx.x,idx.y);
}

////////////////////////////////////////////////////////////////////////////////

Map::Tile & Map::atPixel(float x, float y)
{
    return at(x/m_tileSize, y/m_tileSize);
}

////////////////////////////////////////////////////////////////////////////////

void Map::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
    target.draw(m_sprite);
    util::debugDrawFloatRect(target,states,rect,sf::Color::Red);
}

////////////////////////////////////////////////////////////////////////////////

void Map::update()
{
    static const sf::Vector2f halfTile(m_tileSize/2.0f,m_tileSize/2.0f);

    bool makePlant = util::chance(0.1);
    bool tileChosen = false;

    int lx, ly;
    double largestVal = 0;
    double currentVal;
    for(unsigned x=OCEAN_THRESHOLD; x<m_dim.x-OCEAN_THRESHOLD; ++x)
        for(unsigned y=OCEAN_THRESHOLD; y<m_dim.y-OCEAN_THRESHOLD; ++y)
        {
        	Tile & t = at(x,y);
        	if(t.occupied || t.water)
				continue;

			++t.emptyTicks;

			if(!makePlant)
				continue;

        	currentVal = t.fertility * t.emptyTicks;
        	if(currentVal > largestVal)
			{
				lx = x;
				ly = y;
				largestVal = currentVal;
				tileChosen = true;
			}
        }
	if(tileChosen)
	{
		addPlant(lx,ly);
		at(lx,ly).emptyTicks = 0;
	}
}

const sf::Vector2f & Map::getMainSpawnPoint()
{
    return m_mainSpawnPoint;
}

////////////////////////////////////////////////////////////////////////////////

void Map::setTile(unsigned x, unsigned y, bool water)
{
    if(water)
    {
        m_texture.update(m_waterPixels, m_tileSize, m_tileSize, x*m_tileSize, y*m_tileSize);
        at(x,y).water = true;
    }
    else
    {
        m_texture.update(m_landPixels, m_tileSize, m_tileSize, x*m_tileSize, y*m_tileSize);
        at(x,y).water = false;
    }
}

////////////////////////////////////////////////////////////////////////////////

void Map::initialiseColours()
{
    int tileArraySize = m_tileSize*m_tileSize*4;
    m_waterPixels = new unsigned char[tileArraySize];
    for(int i=0; i<tileArraySize-3; )
    {
        m_waterPixels[i++] = WATER_COLOUR[0];
        m_waterPixels[i++] = WATER_COLOUR[1];
        m_waterPixels[i++] = WATER_COLOUR[2];
        m_waterPixels[i++] = WATER_COLOUR[3];
    }

    m_landPixels = new unsigned char[tileArraySize];
    for(int i=0; i<tileArraySize; )
    {
        m_landPixels[i++] = LAND_COLOUR[0];
        m_landPixels[i++] = LAND_COLOUR[1];
        m_landPixels[i++] = LAND_COLOUR[2];
        m_landPixels[i++] = LAND_COLOUR[3];
    }
}

////////////////////////////////////////////////////////////////////////////////

void Map::initSpecies()
{
    float theta = util::randf(0.0f,2.0f*M_PI);
    float d = std::min(m_dim.x,m_dim.y)/2.0f - OCEAN_THRESHOLD - SPAWN_AREA_RADIUS;
    d *= m_tileSize;

    sf::Vector2f mapCentre(m_dim.x/2.0f, m_dim.y/2.0f);
    mapCentre *= m_tileSize;

    using namespace std;

    Organism * o;
    float theta_dec = 2.0f*M_PI/(float)SpeciesMgr::animalCount();
    sf::Vector2f displacement;
    sf::Vector2f position;
    const SpeciesMgr::SpeciesMap & species = SpeciesMgr::getAll();
    for(SpeciesMgr::SpeciesMap::const_iterator it = species.begin();
        it != species.end();
        ++it)
    {
        if((*it).second->type != Species::ANIMAL) continue;
        displacement.x = d*cos(theta);
        displacement.y = d*sin(theta);
        position = mapCentre + displacement;

        if((*it).second->id == "main")
            m_mainSpawnPoint = position;

        o = new Organism((*it).second);
        o->setPosition(position);
        makeSpawnArea(position);
        EntityMgr::add(o);
        theta += theta_dec;
    }
}

void Map::freeTileAtPixel(const sf::Vector2f & p)
{
    atPixel(p.x,p.y).occupied = false;
}

void Map::makeSpawnArea(const sf::Vector2f & p)
{
    float tx = p.x/m_tileSize;
    float ty = p.y/m_tileSize;

    for(int x=-SPAWN_AREA_RADIUS; x<SPAWN_AREA_RADIUS; ++x)
        for(int y=-SPAWN_AREA_RADIUS; y<SPAWN_AREA_RADIUS; ++y)
        {
            if(util::chance(0.02f))
                addPlant(tx+x,ty+y);

            at(tx+x,ty+y).fertility = util::randf(0.5f,1.0f);
        }
}

void Map::addPlant(int x, int y)
{
    static const sf::Vector2f halfTile(m_tileSize/2.0f,m_tileSize/2.0f);
    Tile & t = at(x,y);
    if(t.occupied || t.water)
        return;
    t.occupied = true;
    Plant * p = new Plant(SpeciesMgr::get("plant"));
    p->setPosition(sf::Vector2f(x*m_tileSize,y*m_tileSize)+ halfTile);
    EntityMgr::add(p);
}
////////////////////////////////////////////////////////////////////////////////
