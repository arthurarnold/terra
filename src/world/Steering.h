//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef STEERING_H
#define STEERING_H

#include "SFML/Graphics.hpp"

class Entity;

namespace steering
{
    sf::Vector2f avoid(const Entity * subject, const Entity * target);
    sf::Vector2f flee(const Entity * subject, const Entity * target);
    sf::Vector2f seek(const Entity * subject, const Entity * target, float stopDistance, float maxAcc);
    sf::Vector2f pursue(const Entity * subject, const Entity * target, float stopDistance, float maxAcc);
    sf::Vector2f wander(const Entity * subject);
}

#endif // STEERING_H

