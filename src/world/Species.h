//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef SPECIES_H
#define SPECIES_H

#include <unordered_set>
#include <SFML/Graphics.hpp>
#include "../ColourManager.h"
#include "../Signal.h"

#include <iostream>

using namespace std;

class Species
{
public:

    enum Type
    {
        PLANT,
        ANIMAL
    };

    // Species identifier
    std::string id;

    Type type = ANIMAL;

    ////////// INDEPENDENT ATTRIBUTES ////////////

    // Lifespan range
    int minLifespan = 4320;
    int maxLifespan = 8640;

    // Organism body colour
    sf::Color colour = ColourMgr::SpeciesDefault;

    float radius = 10.0f;
    float sensorRadius = 100.0f;

    // Maximum amount of food units
    // that can be in stomach
    float stomachHold = 50.0f;

    // Amount of food removed from stomach
    // at every update tick
    float digestionRate = 0.01f;

    // Whether the organisms should fight
    // their predators or flee.
    bool aggressive = false;

    // Damage applied on attack
    float attackDamage = 0.2f;

    // Minimum interval between reproductions
    float reproductionInterval = 720.0f;


    ////////// FACTORS ///////////////////////////

    // For maximum speed
    float gamma = 100.0f;

    // For cruise speed
    float sigma = 0.5f;

    // For maximum acceleration
    float alpha = 0.05f;

    // For eat distance
    float epsilon_e = 1.0f;

    // For attack distance
    float epsilon_a = 2.0f;

    // For eat trigger
    float tau = 0.5f;

    // For minimum reproductive age
    float rho_m = 0.2f;

    // For maximum reproductive age
    float rho_M = 0.6f;

    // For reproduction food level
    float mu = 0.5f;


    ////////// DEPENDANT PROPERTIES //////////////

    inline float maxSpeed() const
    {
        return gamma * digestionRate;
    }

    inline float cruiseSpeed() const
    {
        return sigma * maxSpeed();
    }

    inline float maxAcceleration() const
    {
        return alpha * maxSpeed();
    }

    inline float cruiseDistance() const
    {
        return maxSpeed() / (2.0f*alpha) + radius;
    }

    inline float eatDistance() const
    {
        return epsilon_e + cruiseDistance();
    }

    inline float attackDistance() const
    {
        return epsilon_a + cruiseDistance();
    }

    inline float eatTrigger() const
    {
        return tau * stomachHold;
    }

    inline float reproductionFoodLevel() const
    {
    	return mu*eatTrigger();
    }

    //////////////////////////////////////////////

    bool preysUpon(Species * other) const
    {
        return preys.find(other) != preys.end();
    }

    void addPrey(Species * s)
    {
        preys.insert(s);
    }

    bool isFertile(float age, float lifespan) const
    {
        return ((age >= rho_m*lifespan) &&
                (age <= rho_M * lifespan));
    }

    //////////////////////////////////////////////

    unsigned int getPopulation()
    {
        return m_population;
    }

    void incrementPopulation()
    {
        ++m_population;
        OnPopulationChanged(m_population);
    }

    void decrementPopulation()
    {
        --m_population;
        OnPopulationChanged(m_population);

        if(m_population == 0)
			OnExtinction();
    }

    ////////// SIGNALS ///////////////////////////

    Signal<int> OnPopulationChanged;
    Signal<> OnExtinction;

private:
    // Set of preys
    std::unordered_set<Species *> preys;

    unsigned int m_population;
};

#endif // SPECIES_H
