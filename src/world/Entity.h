//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef ENTITY_H
#define ENTITY_H

#include <list>

#include "Game.h"

class World;

enum EntityType
{
    OBSTACLE,
    ORGANISM,
    FOOD_SOURCE
};

class Entity : public sf::Drawable
{
public:
    Entity(EntityType type) : m_type(type) {}

    EntityType getType() const { return m_type; }

    virtual void setPosition(const sf::Vector2f & pos) { m_position = pos; }

    const sf::Vector2f & getPosition() const { return m_position; }

    // Mandatory implementation:
    virtual float getDistance(const sf::Vector2f & p) const = 0;
    virtual sf::Vector2f getDirectionTo(const sf::Vector2f & p) const = 0;

    // By default, entities have no appearance
    // and are static. These methods must be overridden
    // in order to change this behaviour.
    virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const {}
    virtual bool update() { return true; }

    virtual const sf::Vector2f & getVelocity() const
    {
    	static const sf::Vector2f nullVelocity(0.0f,0.0f);
    	return nullVelocity;
	}

protected:
    sf::Vector2f m_position;

private:
    EntityType m_type;
};

typedef std::list<Entity *> EntityList;

#endif // ENTITY_H
