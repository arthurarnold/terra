//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "World.h"

#include "../Util.h"
#include "../WindowManager.h"
#include "../FontManager.h"
#include "../Animation.h"
#include "SpeciesManager.h"
#include "EntityManager.h"
#include "../GameOverState.h"

using namespace std;

const int MIN_ENTITIES = 10;
const int MAX_ENTITIES = 10;

const float CAM_PAN = 8.0f;

void World::init(float width, float height, bool skipIntro)
{
    m_size = sf::Vector2f(width,height);

    SpeciesMgr::init();

    // Link species to writeData() method so their
    // population changes are printed to their plot file.
    using namespace std::placeholders;
    for(const SpeciesMgr::SpeciesMap::value_type & v : SpeciesMgr::getAll())
    {
        v.second->OnPopulationChanged.connect(
                    std::bind(&World::writeData,this,v.second->id,_1));
        std::string filename = "data/plot/" + v.second->id + ".dat";
        remove(filename.c_str());
    }

    m_map.create(width/10,height/10,10);

    sf::Vector2f viewSize(WindowMgr::getWidth()/12.0f,WindowMgr::getHeight()/12.0f);
    m_view.setSize(viewSize);
	m_view.setCenter(m_map.getMainSpawnPoint());

	if(!skipIntro)
	{
		m_paused = true;
		m_guiEnabled = false;
		m_handleEvents = false;
		initAnimations(27.0f,4.0f);
	}
	else
		initAnimations(2.0f,0.5f);


	m_foreground.setFillColor(sf::Color(245,250,254,0));
	m_foreground.setSize(m_size);

	anim::createSettings("gameOverFGAlpha")
			.duration(m_endMusicDuration)
			.from(0.0f)
			.to(255.0f)
			.onFinish([this]{m_finished=true;});

	SpeciesMgr::get("main")->OnExtinction.connect(std::bind(&World::gameOver,this));
}

void World::cleanup()
{
    EntityMgr::cleanup();
    SpeciesMgr::cleanup();
}

void World::handleEvent(const sf::Event & event, cgf::Game * game)
{
    if(!m_handleEvents)
        return;
    sf::RenderWindow * screen = game->getScreen();
    sf::View previousView = game->getScreen()->getView();
    screen->setView(m_view);

    if(event.type == sf::Event::KeyPressed)
    {
        if(event.key.code == sf::Keyboard::P)
            m_paused = !m_paused;
    }

	if(event.type == sf::Event::Resized)
	{
		m_view.setSize(event.size.width,event.size.height);
		setViewCentre();
		return;
	}

	if(event.type == sf::Event::MouseWheelMoved)
	{
		if(event.mouseWheel.delta > 0)
			zoomIn();
		else
			zoomOut();

        sf::Vector2f point = screen->mapPixelToCoords(
                    sf::Vector2i(event.mouseWheel.x,event.mouseWheel.y),m_view
        );
		setViewCentre(point.x,point.y);
	}

    if(event.type == sf::Event::MouseButtonPressed)
    {
        if(event.mouseButton.button == sf::Mouse::Left)
        {
            sf::Vector2f point = screen->mapPixelToCoords(
                        sf::Vector2i(event.mouseButton.x,event.mouseButton.y)
            );
            EntityMgr::select(point);
        }
    }

    screen->setView(previousView);
}

void World::update(cgf::Game * game)
{
    if(!m_paused)
    {
        ++m_tickCount;
        m_map.update();
        EntityMgr::update();
    }

    if(m_gameOver)
	{
		if(m_finished)
		{
			game->changeState(GameOverState::instance());
		}
	}

    if(!m_handleEvents)
		return;

    sf::Vector2f vec = m_view.getCenter();
	bool flag = false;
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		vec.x -= CAM_PAN;
		flag = true;
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		vec.x += CAM_PAN;
		flag = true;
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		vec.y -= CAM_PAN;
		flag = true;
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		vec.y += CAM_PAN;
		flag = true;
	}

	if(flag)
		setViewCentre(vec.x,vec.y);

	if(sf::Keyboard::isKeyPressed(sf::Keyboard::RBracket))
	{
		zoomIn(0.95f);
		setViewCentre();
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::LBracket))
	{
		zoomOut(1.07f);
		setViewCentre();
    }
}

void World::draw(cgf::Game * game)
{
    sf::RenderWindow & window = *(game->getScreen());
    sf::View previousView = window.getView();
    window.setView(m_view);

    window.draw(m_map);

    EntityMgr::draw(window);

	if(m_gameOver)
	{
		window.draw(m_foreground);
	}

	window.setView(previousView);
}

void World::setViewCentre(float x, float y)
{
	if(x < 0)
		x = m_view.getCenter().x;
	if(y < 0)
		y = m_view.getCenter().y;

	const sf::Vector2f & vsize = m_view.getSize();
	float minX = vsize.x/2.0f,
		  minY = vsize.y/2.0f,
		  maxX = m_size.x - minX,
		  maxY = m_size.y - minY;

	x = std::max(minX, x);
	y = std::max(minY, y);

	x = std::min(maxX, x);
	y = std::min(maxY, y);

	m_view.setCenter(x,y);
}

void World::zoomIn(float factor)
{
	m_view.setSize(factor*m_view.getSize());
}

void World::initAnimations(float duration, float delay)
{
    const sf::Vector2f viewSize = m_view.getSize();
    anim::createSettings("camZoomOutW")
                .duration(duration)
                .startDelay(delay)
                .from(viewSize.x)
                .to(WindowMgr::getWidth())
                .onFinish([this](){
                          m_guiEnabled = true;
                          m_handleEvents = true;
                          m_paused = false;
                    });
    anim::createSettings("camZoomOutH")
                .duration(duration)
                .startDelay(delay)
                .from(viewSize.y)
                .to(WindowMgr::getHeight());

    anim::start(anim::create([this](float x){
                             m_view.setSize(x,m_view.getSize().y);
                             setViewCentre(); },
                             "camZoomOutW"));
    anim::start(anim::create([this](float y){
                             m_view.setSize(m_view.getSize().x,y);
                             setViewCentre(); },
                             "camZoomOutH"));
}

void World::zoomOut(float factor)
{
	sf::Vector2f newSize(factor*m_view.getSize());

	newSize.x = std::min(m_size.x,newSize.x);
	newSize.y = std::min(m_size.y,newSize.y);

	m_view.setSize(newSize);
}

void World::writeData(const std::string & speciesID, int d)
{
    static std::ofstream file;
    file.open("data/plot/"+speciesID+".dat", ios_base::app);
    if(file.is_open())
    {
        file << m_tickCount << "\t" << d << endl;
        file.close();
    }
}

void World::gameOver()
{
	m_gameOver = true;
	m_endMusic.openFromFile("data/audio/music/AasesDeath.ogg");

	auto setForegroundColour = [this](float a) {
		sf::Color c = m_foreground.getFillColor();
		c.a = a;
		m_foreground.setFillColor(c);
	};

	m_guiEnabled = false;
	m_handleEvents = false;
	anim::start(anim::create(setForegroundColour,"gameOverFGAlpha"));
	m_endMusic.play();
}
