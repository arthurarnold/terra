//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef WORLD_H
#define WORLD_H

#include "Game.h"
#include "Entity.h"
#include "Species.h"
#include "Map.h"
#include <list>
#include <memory>
#include <fstream>

class World
{
public:

    void init(float width, float height, bool skipIntro);
    void cleanup();

    const sf::Vector2f & getSize() const { return m_size; }

    void randomiseEntities();

    void handleEvent(const sf::Event & event, cgf::Game * game);

    void update(cgf::Game * game);
    void draw(cgf::Game * game);

    bool isGUIEnabled() { return m_guiEnabled; }

private:
    bool m_paused = false;
    bool m_guiEnabled = true;
    bool m_handleEvents = true;

    // Current view
	sf::View m_view;

    // World dimension in pixels
    sf::Vector2f m_size;


    // Update tick counter
    int m_tickCount;

    void initAnimations(float duration, float delay);

    void zoomIn(float factor = 0.8f);
    void zoomOut(float factor = 1.2f);

	void setViewCentre(float x = -1, float y = -1);

    // Write species population count
    // to its plot file.
    void writeData(const std::string & speciesID, int d);

	void gameOver();
	bool m_gameOver = false;
	bool m_finished = false;
    sf::Music m_endMusic;
    const float m_endMusicDuration = 3.6f;
    sf::RectangleShape m_foreground;

    Map m_map;
};

#endif // WORLD_H
