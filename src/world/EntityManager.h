//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include <unordered_set>
#include "SFML/Graphics.hpp"

class Entity;
class World;

namespace EntityMgr
{
    typedef std::unordered_set<Entity *> Set;

    void add(Entity * entity);
    void remove(Entity * entity);

    void cleanup();

    const Set & getAll();

    int count();

    void draw(sf::RenderWindow & window);
    void update();

    bool exists(Entity * entity);

    // Select organism under point
    void select(const sf::Vector2f & point);
}

#endif // ENTITYMANAGER_H

