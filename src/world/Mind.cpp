//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Mind.h"

#include <cmath>
#include "Sensor.h"
#include "Organism.h"
#include "EntityManager.h"
#include "Steering.h"

Mind::Mind(Organism * parent) :
	m_parent(parent)
{

}

Mind::~Mind()
{

}

void Mind::update()
{
    Organism::Action & action = m_parent->m_action;
    Species & sp = *(m_parent->m_species);

    // CONTINUITY CHECKS
	if(action.type == Organism::Action::EAT)
    {
        // Continue eating if target still exists
        // and if stomach is not yet full.
        if( EntityMgr::exists(action.target) &&
                m_parent->m_foodLevel < sp.stomachHold )
            return;
    }

    if(action.type == Organism::Action::FIGHT)
    {
        // Check if target is still alive.
        if(EntityMgr::exists(action.target))
        {
            // If target is still in attack range, continue fighting it.
            if(action.target->getDistance(m_parent->m_position) <= sp.attackDistance())
                return;
        }
        // If target died, let another action be chosen.
    }
    // END OF CONTINUITY CHECKS

    // Desired velocity vector.
    sf::Vector2f dv(0.0f,0.0f);
    action.type = Organism::Action::MOVE;

    if(avoid_vel(dv))
        goto DV_DEFINED;
    if(flee_vel(dv))
        goto DV_DEFINED;
    if(eat_vel(dv))
        goto DV_DEFINED;
    //////////
    if(reproductionDesired())
    {
        action.type = Organism::Action::REPRODUCE;
        return;
    }
    //////////
    if(prevent_vel(dv))
        goto DV_DEFINED;

    wander_vel(dv);

DV_DEFINED:
//    dv *= sp.maxSpeed;
    action.acc = dv - m_parent->m_velocity;
    util::normalise(action.acc);
    action.acc *= sp.maxAcceleration();
//    util::clamp(action.acc, species->maxSpeed * species->maxAcceleration);
}

bool Mind::avoid_vel(sf::Vector2f & vel)
{
    Entity * co = m_parent->m_sensor->getClosest();
    Species & sp = *(m_parent->m_species);

    if(co == NULL)
        return false;

    if(co->getDistance(m_parent->m_position) > sp.cruiseDistance())
        return false;

    vel = steering::avoid(m_parent,co) * sp.maxSpeed();

    return true;
}

bool Mind::flee_vel(sf::Vector2f & vel)
{
    const EntityList & pr = m_parent->m_sensor->getPredators();
    if(pr.empty())
        return false;

    const float rg = m_parent->m_sensor->getRange();
    const float rd = m_parent->m_species->radius;
    float d;

    float urgency;
    float mag = 0.0f;
    for(Entity * e : pr)
    {
        d = e->getDistance(m_parent->m_position);
        urgency = util::map(d, rg, rd, 0.1f, 1.0f);
        mag = util::accumulate(vel, steering::flee(m_parent,e)*urgency);
        if(mag >= 1.0f)
            break;
//        vel += steering::flee(m_parent,e) * urgency;
    }

    vel *= m_parent->m_species->maxSpeed();

    return mag >= 1.0f;
}

bool Mind::eat_vel(sf::Vector2f & vel)
{
    // Note that when this method is called,
    // the organism is not currently eating.

    if(m_parent->m_foodLevel > m_parent->m_species->eatTrigger())
        return false;

    Sensor & s = *(m_parent->m_sensor);
    Species & sp =*(m_parent->m_species);

    const float ma = sp.maxAcceleration();
    const float er = sp.eatDistance();

    Entity * e = s.getClosestFoodSource();
    float d = s.getDistanceToClosestFoodSource();
    if(e != NULL)
    {
        if(d <= er)
        {
            m_parent->m_action.type = Organism::Action::EAT;
            m_parent->m_action.target = e;
            assert(e->getType() == FOOD_SOURCE);
            return true;
        }

        vel = steering::seek(m_parent,e,er,ma);
        return true;
    }

    const float ar = sp.attackDistance();
    e = m_parent->m_sensor->getClosestPrey();
    d = s.getDistanceToClosestPrey();
    if(e != NULL)
    {
        if(d <= ar)
        {
            m_parent->m_action.type = Organism::Action::FIGHT;
            m_parent->m_action.target = e;
            return true;
        }

        vel = steering::pursue(m_parent,e,ar,ma);
        return true;
    }

    return false;
}

bool Mind::prevent_vel(sf::Vector2f & vel)
{
    const EntityList & all = m_parent->m_sensor->getAll();

    if(all.empty())
        return false;

    const float rg = m_parent->m_sensor->getRange();
    const float cd = m_parent->m_species->cruiseDistance();

    float urgency;
    float d;
    for(Entity * e : all)
    {
        d = e->getDistance(m_parent->m_position);
        urgency = util::map(d, rg, cd, 0.0f, 0.5f);
        vel += steering::avoid(m_parent,e) * urgency;
    }

    return util::clamp(vel,1.0f) >= 0.9f;
}

void Mind::wander_vel(sf::Vector2f & vel)
{
    vel += steering::wander(m_parent) * m_parent->m_species->cruiseSpeed();
}

bool Mind::reproductionDesired()
{
    const Species & sp = *(m_parent->m_species);

    if(!sp.isFertile(m_parent->m_age,m_parent->m_lifespan))
        return false;

    if(m_parent->m_timeSinceLastReproduction < sp.reproductionInterval)
        return false;

    if(m_parent->m_timeSinceLastPredatorSeen < 360)
        return false;

    if(m_parent->m_foodLevel < sp.reproductionFoodLevel())
        return false;

    return true;
}
