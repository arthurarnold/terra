//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef MENTALSTATE_H
#define MENTALSTATE_H

#include "../Util.h"
#include "IntentionGenerator.h"

class MentalState
{
public:
	MentalState()
	{
		m_hunger = 0;
		m_hungerStartTrigger = 30;
		m_hungerStopTrigger = 50;
		m_foodLevel = util::randf(m_hungerStartTrigger,m_hungerStopTrigger);
		m_digestionRate = 0.005;
	}

	void update(const Intention & intention)
	{
		if(m_foodLevel >= m_digestionRate)
			m_foodLevel -= m_digestionRate;
        else
            m_foodLevel = 0.0f;
		// else empty stomach

		calcHunger(intention);
	}

	float getHunger() const { return m_hunger; }
	void incFoodLevel() { m_foodLevel++; }

	float getFoodLevel() const { return m_foodLevel; }

protected:
private:

	////////// Hunger variables //////////

	float m_hunger;
	// [0,1]
	// How badly the organism wishes to eat. This
	// variable is calculated as a function of the
	// ones below.

	float m_foodLevel;
	// The current amount of food in the organism's stomach.

	float m_hungerStartTrigger;
	// How low the food level must get for the organism
	// to want to eat.

	float m_hungerStopTrigger;
	// The maximum food level an organism can have.

	float m_digestionRate;
	// How much food is digested at each update tick.


	////////// Level calculation functions //////////

	void calcHunger(const Intention & intention)
	{
		if(intention.type == EAT)
		{
			// If organism is currently eating, it will
			// only stop if either its stomach gets full
			// or if it can no longer see food around.

			if(m_foodLevel < m_hungerStopTrigger)
				m_hunger = (m_hungerStopTrigger - m_foodLevel) / m_hungerStopTrigger;
			else
				m_hunger = 0.0f;
		}
		else
		{
			// If organism is NOT eating, it will only
			// start if food level drops below the start trigger.

			if(m_foodLevel < m_hungerStartTrigger)
				m_hunger = (m_hungerStartTrigger - m_foodLevel) / m_hungerStartTrigger;
			else
				m_hunger = 0.0f;
		}
	}

	/////////////////////////////////////////////////
};


#endif // MENTALSTATE_H

