//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Game.h"
#include "FontManager.h"
#include "StartState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "WindowManager.h"
#include "Animation.h"

#include <iostream>

#include "AudioManager.h"

using namespace std;

const int WINDOW_W = 1280;
const int WINDOW_H = 720;

#ifdef MAC_OS
#   include <unistd.h>
#endif

int main( int arc, char ** argv )
{
#   ifdef MAC_OS
        char buf[512];
        strncpy(buf,argv[0],sizeof(buf));
        char * last_slash = strrchr(buf,'/');
        last_slash[1] = '\0';
        cout << "[OSX] Will cd into " << buf << endl;
        int res = chdir(buf);
        if(res)
            cout << "[OSX] Failed to change working directory." << endl;
#   endif

    cout << "Config: "
#   ifdef DEBUG
        "Debug"
#   else
        "Release"
#   endif
    << endl;


	WindowMgr::setSize(WINDOW_W,WINDOW_H);

    FontMgr::init();

    cgf::Game game(15,72);
    game.init("Terra", WINDOW_W, WINDOW_H, false, 4);
    game.getScreen()->setMouseCursorVisible(false);

    game.changeState(StartState::instance());

    while(game.isRunning())
    {
        game.handleEvents();
        anim::update();
        game.update();
        game.draw();
    }
	anim::cleanup();
    game.clean();

    // Clean loaded textures
    TextureMgr::cleanup();

    // Clean loaded audio
    AudioMgr::cleanup();

    return 0;
}
