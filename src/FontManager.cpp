//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "FontManager.h"

namespace FontMgr
{
	sf::Font BebasNeue;
	sf::Font Garamond;
	sf::Font Baskerville;
	sf::Font RobotoSlab;
	sf::Font ButtonLabel;
	sf::Font Label;

	void init()
	{
		BebasNeue.loadFromFile("data/fonts/BebasNeue.otf");
		Garamond.loadFromFile("data/fonts/EBGaramond.otf");
		Baskerville.loadFromFile("data/fonts/LibreBaskerville.otf");
		RobotoSlab.loadFromFile("data/fonts/RobotoSlab-Thin.ttf");

		ButtonLabel = BebasNeue;

		Label.loadFromFile("data/fonts/Roboto-Regular.ttf");
	}
}
