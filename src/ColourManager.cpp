//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "ColourManager.h"

namespace ColourMgr
{
    const sf::Color Water = sf::Color(52, 152, 219);

    const sf::Color Button = sf::Color(52, 73, 94);
    const sf::Color ButtonHovered = sf::Color(44, 62, 80);

    const sf::Color Label = sf::Color(44, 62, 80);

    const sf::Color SliderOn = sf::Color(230,74,25);
    const sf::Color SliderOff = sf::Color(150,150,150);

    const sf::Color SpeciesDefault = sf::Color(52,73,94);
}
