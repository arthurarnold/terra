//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include "Game.h"

namespace AudioMgr
{
	void playSound(const std::string & name, float volume = 100.0f);
	void playMusic(const std::string & name, float volume = 100.0f);
	void cleanup();
}

#endif // AUDIO_MANAGER_H

