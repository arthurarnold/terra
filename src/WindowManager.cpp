//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "WindowManager.h"

namespace WindowMgr
{
	float width = 0.0f;
	float height = 0.0f;

	void setSize(float w, float h)
	{
		width = w;
		height = h;
	}

    sf::Vector2f getSize()
    {
    	return sf::Vector2f(width,height);
    }

    float getWidth()
    {
    	return width;
    }

    float getHeight()
    {
    	return height;
    }
}
