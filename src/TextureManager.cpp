//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "TextureManager.h"

namespace TextureMgr
{
	typedef std::map<std::string,sf::Texture *> TextureMap;

	TextureMap textures;

	sf::Texture & get(const std::string & name)
	{
		TextureMap::iterator it = textures.find(name);

		if(it != textures.end())
			return *(it->second);

		sf::Texture * t = new sf::Texture();
		t->loadFromFile(name);

		textures[name] = t;
		return *t;
	}

	void cleanup()
	{
		for(TextureMap::iterator it = textures.begin(); it != textures.end(); ++it)
			delete it->second;
	}
}
