//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef SIGNAL_H
#define SIGNAL_H

#include <functional>
#include <list>
#include <map>

template<class... Args>
class Signal
{
public:
	typedef unsigned int ID;
	typedef std::function<void(Args...)> DelegateFunc;

	void connect(DelegateFunc delegate)
	{
		m_delegates.push_back(delegate);
	}

	void operator () (Args... args)
	{
		for(const DelegateFunc & delegate : m_delegates)
			delegate(args...);
	}

private:
	typedef std::list<DelegateFunc> DelegateList;
	DelegateList m_delegates;
};


#endif // SIGNAL_H
