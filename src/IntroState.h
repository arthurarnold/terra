//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef INTROSTATE_H
#define INTROSTATE_H

#include "GameState.h"
#include "SFML/Audio.hpp"
#include "gui/Button.h"
#include "Animation.h"
#include <list>

class IntroState : public cgf::GameState
{
public:
    void init() override;
    void cleanup() override;

    void pause() override;
    void resume() override;

    void handleEvents(cgf::Game * game) override;

    void update(cgf::Game * game) override;

    void draw(cgf::Game * game) override;

    static IntroState * instance()
    {
        return &m_instance;
    }

protected:
    IntroState() {}

private:
    static IntroState m_instance;

    std::list<sf::Text *> m_texts;
    std::list<anim::Handle> m_animHandles;

    sf::Text m_skipText;

    sf::Music m_music;

    sf::Text * makeText(const std::string & string,
                        const sf::Vector2f & position,
                        int size = -1);

    const float m_switchTime = 31.74f;
};


#endif // INTROSTATE_H
