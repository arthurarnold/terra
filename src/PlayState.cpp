//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "PlayState.h"

#include <iostream>

#include "Animation.h"
#include "Util.h"
#include "WindowManager.h"

PlayState PlayState::m_playState;

#define INIT_NUM_ORGANISMS 10
#define INIT_NUM_FOODSOURCES 30

using namespace std;

void PlayState::init()
{
    m_im = cgf::InputManager::instance();
    m_im->addKeyInput("esc", sf::Keyboard::Escape);
    m_im->addKeyInput("left", sf::Keyboard::Left);
    m_im->addKeyInput("right", sf::Keyboard::Right);
    m_im->addKeyInput("up", sf::Keyboard::Up);
    m_im->addKeyInput("down", sf::Keyboard::Down);

    m_im->addMouseInput("zoom", sf::Mouse::Middle);

    m_world.init(WindowMgr::getWidth()*2.0f, WindowMgr::getHeight()*2.0f, m_skipIntro);
//    m_world.randomiseEntities();

    m_gui.init();
}

void PlayState::cleanup()
{
    m_world.cleanup();
}

void PlayState::setSkipIntro(bool skip)
{
	m_skipIntro = skip;
}

void PlayState::pause()
{
}

void PlayState::resume()
{
}

void PlayState::handleEvents(cgf::Game * game)
{
    sf::RenderWindow * screen = game->getScreen();
    sf::Event event;

    while (screen->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            game->quit();
		if(event.type == sf::Event::Resized)
			WindowMgr::setSize(event.size.width,event.size.height);

        if(event.type == sf::Event::KeyPressed)
        {
			if(event.key.code == sf::Keyboard::S)
                game->toggleStats();
        }
		m_world.handleEvent(event,game);
		m_gui.handleEvent(event);
    }

    if(m_im->testEvent("esc"))
        game->quit();
}

void PlayState::update(cgf::Game * game)
{
    m_world.update(game);
    if(m_world.isGUIEnabled())
        m_gui.update(game);
}

void PlayState::draw(cgf::Game * game)
{
    sf::RenderTarget * screen = game->getScreen();

    //screen->clear(sf::Color(52, 152, 219));
    screen->clear(sf::Color(245,250,254));

    m_world.draw(game);
    if(m_world.isGUIEnabled())
        m_gui.draw(game);
}
