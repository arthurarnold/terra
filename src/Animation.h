//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef ANIMATION_H
#define ANIMATION_H

#include <string>
#include <functional>

namespace anim
{
    // Arguments (percentage, from, to).
    typedef std::function<float(float,float,float)> InterpolationFunction;

    // Animation end callback
    typedef std::function<void()> OnFinishFunction;
    typedef std::function<void(float)> OnUpdateFunction;

    typedef std::function<void(float)> SetterFunction;

    typedef unsigned Handle;

    extern const InterpolationFunction linear;
    extern const InterpolationFunction easeInOut;

    extern const int REPEAT_FOREVER;


    class Settings
    {

    public:
        Settings() {}
        //////////////////////////////////////////
        Settings & from(float f) { m_from = f; return *this; }
        float from() const { return m_from; }
        //////////////////////////////////////////
        Settings & to(float t) { m_to = t; return *this; }
        float to() const { return m_to; }
        //////////////////////////////////////////
        Settings & duration(float d) { m_duration = d; return *this; }
        float duration() const { return m_duration; }
        //////////////////////////////////////////
        Settings & startDelay(float d) { m_startDelay = d; return *this; }
        float startDelay() const { return m_startDelay; }
        //////////////////////////////////////////
        Settings & interpolationFunction(const InterpolationFunction & f) { m_interpolationFunction = f; return *this; }
        const InterpolationFunction & interpolationFunction() const { return m_interpolationFunction; }
        //////////////////////////////////////////
        Settings & repeat(int times, bool bounce = false)
        {
            m_repeatCount = times;
            m_bounce = bounce;
            return *this;
        }
        int repeatCount() const { return m_repeatCount; }
        bool bounce() const { return m_bounce; }
        //////////////////////////////////////////
        Settings & repeatDelay(float d)
        {
            m_repeatDelay= d;
            return *this;
        }
        float repeatDelay() const { return m_repeatDelay; }
        //////////////////////////////////////////
        Settings & onFinish(const OnFinishFunction & f)
        {
            m_onFinish = f;
            return *this;
        }
        const OnFinishFunction & onFinish() const { return m_onFinish; }
        //////////////////////////////////////////
        Settings & onUpdate(const OnUpdateFunction & f)
        {
            m_onUpdate = f;
            return *this;
        }
        const OnUpdateFunction & onUpdate() const { return m_onUpdate; }

    private:

        float m_from;
        float m_to;
        float m_duration;
        float m_startDelay = 0.0f;

        ::anim::InterpolationFunction m_interpolationFunction = ::anim::easeInOut;

        // Repetition configuration
        int m_repeatCount = 0;
        bool m_bounce = false;
        float m_repeatDelay = 0.0f;

        OnFinishFunction m_onFinish;
        OnUpdateFunction m_onUpdate;

        float m_start;
    };

    Settings & createSettings(const std::string & name);
    Settings & getSettings(const std::string & name);

    bool hasSettings(const std::string & name);

    Handle create(float * var, const std::string & settingsName);
    Handle create(const SetterFunction & setter, const std::string & settingsName);

    void destroy(Handle h);

    void start(Handle h);
    void pause(Handle h);

    void update();
    void cleanup();
}

#endif // ANIMATION_H

