//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Util.h"

#include <algorithm>
#include <cmath>
#include <iostream>
using namespace std;

namespace util
{
    std::default_random_engine gen;
    std::uniform_real_distribution<double> dist;

	const float sqrt2 = sqrt(2);
    const float sqrt3 = sqrt(3);

    float length(const sf::Vector2f & v)
    {
        return sqrt(v.x*v.x + v.y*v.y);
    }

    unsigned length(const sf::Vector2u & v)
    {
        return sqrt(v.x*v.x + v.y*v.y);
    }

    bool zero(const sf::Vector2f & v)
    {
        return v.x == 0.0f && v.y == 0.0f;
    }

    void normalise(sf::Vector2f & v)
    {
        float l = length(v);

        if(l == 0)
            return;

        v.x /= l;
        v.y /= l;
    }

    sf::Vector2f norm(const sf::Vector2f & v)
    {
        float l = length(v);

        if(l == 0)
            return sf::Vector2f(0.0f,0.0f);

        return sf::Vector2f(v.x/l, v.y/l);
    }

    void setAngle(sf::Vector2f & v, float angle)
    {
        float l = length(v);
        v.x = cos(angle) * l;
        v.y = sin(angle) * l;
    }

    void rotate(sf::Vector2f & v, float angle)
    {
        float s = sin(angle);
        float c = cos(angle);

        float x2 = v.x*c - v.y*s;
        float y2 = v.x*s + v.y*c;

        v.x = x2;
        v.y = y2;
    }

    void truncate(sf::Vector2f & v, float max)
    {
        if(length(v) > max)
        {
            normalise(v);
            v *= max;
        }
    }

    float sqdist(const sf::Vector2f & v1, const sf::Vector2f & v2)
    {
    	return (v1.x-v2.x)*(v1.x-v2.x) + (v1.y-v2.y)*(v1.y-v2.y);
    }

    float accumulate(sf::Vector2f & accum, const sf::Vector2f & value)
    {
        float magLeft = 1.0f - util::length(accum);
        float vMag = util::length(value);
        float newMag = vMag < magLeft ? vMag : magLeft;

        accum += (util::norm(value) * newMag);

        return newMag;
    }

    float clamp(sf::Vector2f & value, float max)
    {
        float len = length(value);
        if(len > max)
        {
            value *= max/len;
            return max;
        }

        return len;
    }

    ////////////////////////////////////////////////////////////////////////////

    float randf(float lb, float ub)
    {
        return lb + dist(gen)*(ub-lb);
    }

    int randi(int lb, int ub)
    {
        return (int)randf((float)lb,(float)ub);
    }

    unsigned char randub()
    {
        return randf() * 256;
    }

    sf::Vector2f rand2f(const sf::Vector2f & lb, const sf::Vector2f & ub)
    {
        return sf::Vector2f(randf(lb.x,ub.x), randf(lb.y,ub.y));
    }

    bool chance(float c)
    {
        return (randf() <= c);
    }

    sf::FloatRect addFloatRects(const sf::FloatRect & fr1, const sf::FloatRect & fr2, bool keepOrigin)
    {
    	sf::FloatRect ret;
    	if(keepOrigin)
		{
			ret.left = fr1.left;
			ret.top = fr1.top;
		}
		else
		{
			ret.left = std::min(fr1.left,fr2.left);
			ret.top = std::min(fr1.top, fr2.top);
		}

    	float w1 = fr1.left + fr1.width - ret.left,
			  h1 = fr1.top + fr1.height - ret.top,
			  w2 = fr2.left + fr2.width - ret.left,
			  h2 = fr2.top + fr2.height - ret.top;

		ret.width = std::max(w1,w2);
		ret.height = std::max(h1,h2);

		return ret;
    }

    void debugDrawFloatRect(sf::RenderTarget & target,
                            sf::RenderStates state,
                            const sf::FloatRect & rect,
                            const sf::Color & colour)
    {
#		ifdef DEBUG
			sf::RectangleShape rs(sf::Vector2f(rect.width, rect.height));
			rs.setPosition(rect.left, rect.top);
			rs.setFillColor(sf::Color::Transparent);
			rs.setOutlineThickness(1);
			rs.setOutlineColor(colour);
            target.draw(rs,state);
#		endif
    }

    float map(float value, float fromMin, float fromMax, float toMin, float toMax)
    {
    	return ((value-fromMin)/(fromMax-fromMin))*(toMax-toMin) + toMin;
    }

    void memset(void * dst, size_t dsize, void * const src, size_t ssize)
    {
        unsigned char * d = (unsigned char *) dst;
        unsigned char * s = (unsigned char *) src;

        for(size_t n=0; n<dsize; ++n)
            d[n] = s[n%ssize];
    }

    sf::Event makeLocalEvent(const sf::Event & event, const sf::Vector2f & origin)
    {
        sf::Event localEvent = event;
        switch(localEvent.type)
        {
        case sf::Event::MouseWheelMoved:
            localEvent.mouseWheel.x -= origin.x;
            localEvent.mouseWheel.y -= origin.y;
            break;
        case sf::Event::MouseButtonPressed:
        case sf::Event::MouseButtonReleased:
            localEvent.mouseButton.x -= origin.x;
            localEvent.mouseButton.y -= origin.y;
            break;
        case sf::Event::MouseMoved:
            localEvent.mouseMove.x -= origin.x;
            localEvent.mouseMove.y -= origin.y;
            break;
        default:
            break;
        }

        return localEvent;
    }
}

