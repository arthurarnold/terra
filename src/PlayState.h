//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <list>

#include "GameState.h"
#include "InputManager.h"
#include "Sprite.h"

#include "world/World.h"
#include "gui/GUI.h"

class PlayState : public cgf::GameState
{
public:
    void init() override;
    void cleanup() override;

    void setSkipIntro(bool skip);

    void pause() override;
    void resume() override;

    void handleEvents(cgf::Game * game) override;

    void update(cgf::Game * game) override;

    void draw(cgf::Game * game) override;

    static PlayState * instance()
    {
        return &m_playState;
    }

    World * getWorld() { return &m_world; }

protected:
    PlayState() {}

private:
    static PlayState m_playState;

    bool m_skipIntro = false;

    World m_world;

    GUI m_gui;

    cgf::InputManager * m_im;

};

#endif // PLAYSTATE_H

