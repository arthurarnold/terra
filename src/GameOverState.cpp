//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "GameOverState.h"

#include "Animation.h"
#include "FontManager.h"
#include "WindowManager.h"
#include "ColourManager.h"
#include <iostream>
using namespace std;

GameOverState GameOverState::m_instance;


void GameOverState::init()
{
	m_gameOverText.setFont(FontMgr::BebasNeue);
	m_gameOverText.setCharacterSize(30);
	m_gameOverText.setString("Extinction");
	sf::FloatRect r = m_gameOverText.getGlobalBounds();
	m_gameOverText.setOrigin( (int)(r.left+r.width/2.0f) , (int)(r.top+r.height/2.0f) );
	m_gameOverText.setPosition(WindowMgr::getSize()*0.5f);
	sf::Color c = ColourMgr::Label;
	c.a = 0;
	m_gameOverText.setColor(c);

	anim::createSettings("gameOverTextAlpha")
			.duration(1.0f)
			.from(0.0f)
			.to(255.0f);

	auto setTextColour = [this](float a) {
		sf::Color c = m_gameOverText.getColor();
		c.a = a;
		m_gameOverText.setColor(c);
	};


	anim::start(anim::create(setTextColour,"gameOverTextAlpha"));
}

void GameOverState::cleanup()
{

}

void GameOverState::pause()
{

}

void GameOverState::resume()
{

}

void GameOverState::handleEvents(cgf::Game * game)
{
	sf::RenderWindow * screen = game->getScreen();
    sf::Event event;

    while (screen->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            game->quit();
        if(event.type == sf::Event::KeyPressed)
        {
            if(event.key.code == sf::Keyboard::Escape)
                game->quit();
        }
    }
}

void GameOverState::update(cgf::Game * game)
{

}

void GameOverState::draw(cgf::Game * game)
{
    sf::RenderWindow & screen = *(game->getScreen());
    screen.clear(sf::Color(245,250,254));

    screen.draw(m_gameOverText);
}
