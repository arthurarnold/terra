//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Animation.h"

#include <map>
#include <iostream>
#include "SFML/System.hpp"
#include "Util.h"

float interp_linear(float p, float from, float to)
{
    p *= 2.0f;
    if(p < 1.0f) return (to-from)/2.0f*p*p*p + from;
    p -= 2.0f;
    return (to-from)/2.0f*(p*p*p + 2.0f) + from;
}

float interp_easeInOut(float p, float from, float to)
{
    return -2.0f*(to-from)*p*p*p + 3.0f*(to-from)*p*p + from;
}


namespace anim
{
    Handle highestHandleAssigned = 0;

    const InterpolationFunction linear = interp_linear;
    const InterpolationFunction easeInOut = interp_easeInOut;

    const int REPEAT_FOREVER = -1;


    ////////////////////////////////////////////////////////////////////////////


    class Animation
    {
    public:
        Animation(float * var, const Settings & settings) :
            m_settings(settings),
            m_var(var)
        {

        }

        Animation(const SetterFunction & setter, const Settings & settings) :
            m_settings(settings),
            m_setter(setter)
        {

        }

        void start() { m_running = true; }
        void pause() { m_running = false; }
        bool running() { return m_running; }

        //////////////////////////////////////////

        void update(float secondsElapsed)
        {
            if(!m_running) return;

            m_currentTime += secondsElapsed;
            if(m_currentTime > m_settings.startDelay())
            {
                float actualTime = m_currentTime - m_settings.startDelay();
                if(actualTime >= m_settings.duration())
                {
                    // Finished.
                    setVarOrCallSetter(m_settings.interpolationFunction()(
                                           1.0f,
                                           m_settings.from(),
                                           m_settings.to()));
                    if(m_settings.onFinish())
                        m_settings.onFinish()();
                    m_running = false;
                    m_currentTime = 0.0f;
                    return;
                }

                float timePercentage = actualTime / m_settings.duration();
                setVarOrCallSetter(m_settings.interpolationFunction()(
                                       timePercentage,
                                       m_settings.from(),
                                       m_settings.to()));
            }
        }

    private:
        Settings m_settings;
        float * m_var = NULL;
        float m_currentTime = 0.0f;
        SetterFunction m_setter;
        bool m_running = false;

        void setVarOrCallSetter(float v)
        {
            if(m_var != NULL)
                (*m_var) = v;
            else if(m_setter)
                m_setter(v);
            if(m_settings.onUpdate())
                m_settings.onUpdate()(v);
        }
    };


    ////////////////////////////////////////////////////////////////////////////


    typedef std::map<Handle, Animation *> AnimationMap;
    AnimationMap animationMap;

    typedef std::map<std::string, Settings *> SettingsMap;
    SettingsMap settingsMap;

    ////////// BEGINNING OF PUBLIC INTERFACE ///////////////////////////////////

    Settings & createSettings(const std::string & name)
    {
        SettingsMap::iterator it = settingsMap.find(name);
        if(it != settingsMap.end())
            return *(it->second);

        Settings * s = new Settings();
        settingsMap[name] = s;
        return *s;
    }

    Settings & getSettings(const std::string & name)
    {
        static Settings emptySettings;

        SettingsMap::iterator it = settingsMap.find(name);
        if(it != settingsMap.end())
            return *(it->second);
        std::cout << "[Animation] WARNING: settings '" << name << "' do not exist." << std::endl;
        return emptySettings;
    }

    bool hasSettings(const std::string & name)
    {
        return settingsMap.find(name) != settingsMap.end();
    }

    Handle create(float * var, const std::string & settingsName)
    {
        Handle handle = highestHandleAssigned;
        ++highestHandleAssigned;
        animationMap[handle] = new Animation(var,getSettings(settingsName));
        return handle;
    }

    Handle create(const SetterFunction & setter, const std::string & settingsName)
    {
        Handle handle = highestHandleAssigned;
        ++highestHandleAssigned;
        animationMap[handle] = new Animation(setter,getSettings(settingsName));
        return handle;
    }

    void destroy(Handle h)
    {
        AnimationMap::iterator it = animationMap.find(h);
        if(it != animationMap.end())
        {
            delete it->second;
            animationMap.erase(it);
        }
    }

    void start(Handle h)
    {
        AnimationMap::iterator it = animationMap.find(h);

        if(it != animationMap.end())
            it->second->start();
    }

    void pause(Handle h)
    {
        AnimationMap::iterator it = animationMap.find(h);
        if(it != animationMap.end())
            it->second->pause();
    }

    ////////////////////////////////////////////////////////////////////////////

    sf::Clock clock;

    void update()
    {
        float timeElapsed = clock.getElapsedTime().asSeconds();

        for(AnimationMap::iterator it = animationMap.begin();
            it != animationMap.end(); ++it)
        {
            it->second->update(timeElapsed);
        }

        clock.restart();
    }

    void cleanup()
    {
        for(AnimationMap::iterator it = animationMap.begin();
            it != animationMap.end();
            ++it)
        {
            delete it->second;
        }

        for(SettingsMap::iterator it = settingsMap.begin();
            it != settingsMap.end();
            ++it)
        {
            delete it->second;
        }
    }
}
