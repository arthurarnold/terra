//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef GAME_OVER_STATE_H
#define GAME_OVER_STATE_H

#include "GameState.h"

class GameOverState : public cgf::GameState
{
public:
    void init() override;
    void cleanup() override;

    void pause() override;
    void resume() override;

    void handleEvents(cgf::Game * game) override;

    void update(cgf::Game * game) override;

    void draw(cgf::Game * game) override;

    static GameOverState * instance()
    {
        return &m_instance;
    }

protected:
    GameOverState() {}

private:
    static GameOverState m_instance;

    sf::Text m_gameOverText;
};

#endif // GAME_OVER_STATE_H

