//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "IntroState.h"

#include "WindowManager.h"
#include "Animation.h"
#include "AudioManager.h"
#include "PlayState.h"
#include <iostream>
using namespace std;

IntroState IntroState::m_instance;

const std::string strings[] = {
    "life",
    "noun",
    "The condition that distinguishes animals and plants from inorganic matter, ",
    "including the capacity for growth, ",
    "reproduction, ",
    "functional activity, ",
    "and continual change preceding death."
};

const float delays[] = {
    4.0f,
    4.0f,
    8.0f,
    12.0f,
    16.0f,
    19.5f,
    23.5f
};

void IntroState::init()
{
    sf::Text * t;
    sf::Vector2f displacement;
    sf::Vector2f origin(WindowMgr::getSize()*0.2f);

    t = makeText(strings[0],origin);

    displacement.y = 40.0f;
    t = makeText(strings[1],t->getPosition() + displacement,14);
    t->setStyle(sf::Text::Italic);

    t = makeText(strings[2],t->getPosition() + displacement, 18);
    displacement.y = t->getGlobalBounds().height + 10.0f;
    t = makeText(strings[3],t->getPosition() + displacement, 18);

    displacement.x = t->getGlobalBounds().width;
    displacement.y = 0;
    t = makeText(strings[4],t->getPosition() + displacement, 18);

    displacement.x = t->getGlobalBounds().width;
    displacement.y = 0;
    t = makeText(strings[5],t->getPosition() + displacement, 18);

    displacement.x = origin.x;
    displacement.y = t->getPosition().y + t->getGlobalBounds().height + 10.0f;
    t = makeText(strings[6],displacement, 18);

    anim::createSettings("textFadeIn")
        .duration(0.5f)
        .from(0.0f)
        .to(255.0f);
    anim::createSettings("textFadeOut")
        .duration(2.0f)
        .from(255.0f)
        .to(0.0f)
        .startDelay(29.0f);

    std::function<void(sf::Text *, float)> colourSetter = [](sf::Text * t, float v) {
        sf::Color c = t->getColor();
        c.a = v;
        t->setColor(c);
    };

    using namespace std::placeholders;

    int i = 0;
    anim::Handle handle;
    for(sf::Text * t : m_texts)
    {
        std::function<void(float)> tSetter = std::bind(colourSetter,t,_1);
        anim::getSettings("textFadeIn")
            .startDelay(delays[i]);

		handle = anim::create(tSetter, "textFadeIn");
		m_animHandles.push_back(handle);
        anim::start(handle);

        handle = anim::create(tSetter, "textFadeOut");
        m_animHandles.push_back(handle);
        anim::start(handle);
        ++i;
    }

	m_skipText.setFont(FontMgr::BebasNeue);
	m_skipText.setCharacterSize(18);
	m_skipText.setColor(ColourMgr::Label);
	m_skipText.setString("Press S to skip intro");
	m_skipText.setPosition(10.0f, WindowMgr::getHeight()-m_skipText.getGlobalBounds().height-10.0f);

    m_music.openFromFile("data/audio/music/Symphony7-II.ogg");
    m_music.play();
}

void IntroState::cleanup()
{
    for(sf::Text * t : m_texts)
        delete t;

	for(anim::Handle & handle : m_animHandles)
		anim::destroy(handle);
}

void IntroState::pause()
{

}

void IntroState::resume()
{

}

void IntroState::handleEvents(cgf::Game * game)
{
    sf::RenderWindow * screen = game->getScreen();
    sf::Event event;

    while (screen->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            game->quit();
        if(event.type == sf::Event::KeyPressed)
        {
            if(event.key.code == sf::Keyboard::Escape)
                game->quit();
			if(event.key.code == sf::Keyboard::S)
			{
				m_music.stop();
				PlayState::instance()->setSkipIntro(true);
				game->changeState(PlayState::instance());
			}
        }
    }
}

void IntroState::update(cgf::Game * game)
{
    float s = m_music.getPlayingOffset().asSeconds();
    if(s >= m_switchTime)
        game->changeState(PlayState::instance());
}

void IntroState::draw(cgf::Game * game)
{
    sf::RenderWindow & screen = *(game->getScreen());
    screen.clear(sf::Color(245,250,254));

    screen.draw(m_skipText);

    for(sf::Text * t : m_texts)
        screen.draw(*t);
}

sf::Text * IntroState::makeText(
    const std::string & str,
    const sf::Vector2f & position,
    int size)
{
    static sf::Color colour = ColourMgr::Label;
    colour.a = 0;
    sf::Text * t = new sf::Text();
    t->setFont(FontMgr::Baskerville);
    t->setColor(colour);
    t->setString(str);
    t->setPosition(position);
    if(size > 0)
        t->setCharacterSize(size);

    m_texts.push_back(t);
    return t;
}
