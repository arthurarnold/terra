//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef WINDOW_MANAGER_H
#define WINDOW_MANAGER_H

#include "Game.h"

namespace WindowMgr
{
    void setSize(float w, float h);

    sf::Vector2f getSize();
    float getWidth();
    float getHeight();
}

#endif // WINDOW_MANAGER_H

