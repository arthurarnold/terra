//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "StartState.h"
#include "WindowManager.h"
#include "IntroState.h"
#include "PlayState.h"
#include "AudioManager.h"
#include <iostream>

using namespace std;

StartState StartState::m_instance;

void StartState::init()
{
    bebas.loadFromFile("data/fonts/BebasNeue.otf");

    m_txt.setFont(bebas);
    m_txt.setString("Press any key to start");
    m_txt.setCharacterSize(30);
    m_txt.setColor(sf::Color(52, 73, 94));

    sf::FloatRect r = m_txt.getLocalBounds();
    m_txt.setOrigin((int)(r.left+r.width/2.0), (int)(r.top+r.height/2.0));
    m_txt.setPosition((int)(WindowMgr::getWidth()/2.0f), (int)(WindowMgr::getHeight()/2.0f));
}

void StartState::cleanup()
{}

void StartState::pause()
{}

void StartState::resume()
{}

void StartState::handleEvents(cgf::Game * game)
{
    sf::RenderWindow * screen = game->getScreen();
    sf::Event event;

    while (screen->pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            game->quit();
        if(event.type == sf::Event::KeyPressed)
        {
            if(event.key.code == sf::Keyboard::Escape)
                game->quit();
            else
//                game->changeState(PlayState::instance());
                game->changeState(IntroState::instance());
        }

    }
}

void StartState::update(cgf::Game * game)
{}

void StartState::draw(cgf::Game * game)
{
    game->getScreen()->clear(sf::Color(245,250,254));

    game->getScreen()->draw(m_txt);
}
