//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef COLOUR_MANAGER_H
#define COLOUR_MANAGER_H

#include "Game.h"

namespace ColourMgr
{
    extern const sf::Color Water;

    extern const sf::Color Button;
    extern const sf::Color ButtonHovered;

    extern const sf::Color Label;

    extern const sf::Color SliderOn;
    extern const sf::Color SliderOff;

    extern const sf::Color SpeciesDefault;
}

#endif // COLOUR_MANAGER_H

