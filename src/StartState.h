//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef STARTSTATE_H
#define STARTSTATE_H

#include "GameState.h"
#include "Sprite.h"

class StartState : public cgf::GameState
{
public:
    void setWindowSize(int w, int h);

    void init() override;
    void cleanup() override;

    void pause() override;
    void resume() override;

    void handleEvents(cgf::Game * game) override;
    void update(cgf::Game * game) override;
    void draw(cgf::Game * game) override;

    static StartState * instance()
    {
        return &m_instance;
    }

protected:

    StartState() : m_width(-1), m_height(-1) {}

private:

    static StartState m_instance;

    int m_width, m_height;

    sf::Font bebas;
    sf::Text m_txt;

};


#endif
