//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef UTIL_H
#define UTIL_H

#include "Game.h" // sfml
#include <random>

namespace util
{
	extern const float sqrt2;
	extern const float sqrt3;

	///// VECTORS ////////////////////////////////

		// Vector length.
		float length(const sf::Vector2f & v);
        unsigned length(const sf::Vector2u & v);

		// Whether vector has length zero.
		bool zero(const sf::Vector2f & v);

		// In-place vector normalisation.
		void normalise(sf::Vector2f & v);

		// Returns a vector's norm.
		sf::Vector2f norm(const sf::Vector2f & v);

		// Sets a vector's angle, maintaining length.
		void setAngle(sf::Vector2f & v, float angle);

		// Rotates a vector.
		void rotate(sf::Vector2f & v, float angle);

		// Truncates vector length to within max.
		void truncate(sf::Vector2f & v, float max);

		// Returns the squared distance between
		// two points (to avoid computing square root).
		float sqdist(const sf::Vector2f & v1, const sf::Vector2f & v2);

        // Truncates value so that, when added to the accumulator, the latter
        // will have magnitude not greater than 1.0. It adds the truncated
        // value and returns the accumulator's final magnitude.
        float accumulate(sf::Vector2f & accum, const sf::Vector2f & value);

        float clamp(sf::Vector2f & value, float max);

    ///// RANDOM NUMBERS /////////////////////////

		// Random float in interval.
		float randf(float lb = 0.0f, float ub = 1.0f);

		// Random integer in interval.
		int randi(int lb = 0, int ub = RAND_MAX);

		// Random unsigned byte in interval.
		unsigned char randub();

		// Random vector in interval.
		sf::Vector2f rand2f(const sf::Vector2f & lb = sf::Vector2f(0.0f,0.0f),
							const sf::Vector2f & ub = sf::Vector2f(1.0f,1.0f));

        bool chance(float c);


	///// FLOAT RECTS ////////////////////////////

		// Returns the smallest FloatRect containing
		// both parameters.
		sf::FloatRect addFloatRects(const sf::FloatRect & fr1, const sf::FloatRect & fr2, bool keepOrigin = false);

		// Draws a FloatRect.
        void debugDrawFloatRect(sf::RenderTarget & target, sf::RenderStates state, const sf::FloatRect & rect, const sf::Color & colour = sf::Color::Red);

	//////////////////////////////////////////////

	float map(float value, float fromMin, float fromMax, float toMin=0.0f, float toMax=1.0f);

    void printf(const char * fmt, ...);

    void memset(void * dst, size_t dsize, void * src, size_t ssize);

    sf::Event makeLocalEvent(const sf::Event & event, const sf::Vector2f & origin);
}

#endif // UTIL_H

