//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef FONTS_H
#define FONTS_H

#include "Game.h"

namespace FontMgr
{
	void init();

	extern sf::Font BebasNeue;
	extern sf::Font Garamond;
	extern sf::Font Baskerville;
	extern sf::Font RobotoSlab;
	extern sf::Font ButtonLabel;
	extern sf::Font Label;
}

#endif // FONTS_H

