//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "AudioManager.h"

#include <iostream>

using namespace std;

namespace AudioMgr
{
	typedef std::map<std::string,sf::Sound *> SoundMap;
	SoundMap sounds;

//	typedef std::map<std::string,sf::Music *> MusicMap;
//	MusicMap music;

	const std::string BASE_PATH = "data/audio/";

	sf::Sound * getSound(const std::string & name)
	{
		SoundMap::iterator it = sounds.find(name);

		if(it != sounds.end())
			return it->second;

		sf::SoundBuffer * buf = new sf::SoundBuffer();
		buf->loadFromFile(BASE_PATH+"sounds/" + name + ".ogg");

		sf::Sound * sound = new sf::Sound();
		sound->setBuffer(*buf);
		sounds[name] = sound;
		return sound;
	}

	void playSound(const std::string & name, float volume)
	{
        SoundMap::iterator it = sounds.find(name);
        sf::Sound * s = getSound(name);

        s->setVolume(volume);
        s->play();
	}


//	sf::Music * getMusic(const std::string & name)
//	{
//	    MusicMap::iterator it = music.find(name);
//
//	    if(it != music.end())
//            return it->second;
//
//        sf::Music * m = new sf::Music();
//        if(!m->openFromFile(BASE_PATH+"music/" + name + ".ogg"))
//            cout << "[AudioMgr] ERROR: failed to open '" << name << "'" << endl;
//
//        music[name] = m;
//        return m;
//	}
//
//	void playMusic(const std::string & name, float volume)
//	{
//	    sf::Music m;
//	    if(!m.openFromFile(BASE_PATH+"music/"+name+".ogg"))
//            cout << "[AudioMgr] ERROR: failed to open '" << name << "'" << endl;
//        m.play();
//	}


	void cleanup()
	{
	    cout << "AudioManager: cleaning " << sounds.size() << " sound object(s)" << endl;
        for(SoundMap::iterator it = sounds.begin(); it != sounds.end(); ++it)
        {
            delete it->second->getBuffer();
            delete it->second;
        }
	}
}
