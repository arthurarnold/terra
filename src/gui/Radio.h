//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef RADIO_H
#define RADIO_H

#include "Control.h"
#include "Container.h"
#include "Label.h"

class Radio : public Control
{
public:
	Radio(const std::string & label = "", bool checked = false);
	~Radio() = default;

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

	void onMouseLeftDown(float x, float y) override;
    void onPositionChange() override;

	bool checked();
	void check();
	void uncheck();

	void setLabel(const std::string & label);

	operator bool();

private:

	bool m_checked;

	sf::Sprite m_sprite;

	Label m_label;

	void updateSprite();
};


////////////////////////////////////////////////////////////////////////////////


class RadioGroup : public Container<Radio>
{
public:
	RadioGroup();

	// Override from Container
	void add(Radio * r) override;

	// Override from Container and use
	// implementation from Control.
	bool handleEvent(const sf::Event & event) override;

	// Override from Control
	void onMouseLeftDown(float x, float y) override;

    void onPositionChange() override;

	// Get currently checked radio button
	// in this group.
	Radio * getSelected();

private:
	Radio * m_selected;

	float m_largestY;
};

#endif // RADIO_H

