//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Checkbox.h"

#include "../TextureManager.h"
#include "../Util.h"

#include <iostream>
using namespace std;

Checkbox::Checkbox(const std::string & label) :
	m_checked(false)
{
	updateSprite();
    setSize((sf::Vector2f)m_sprite.getTexture()->getSize());
    m_label.setPosition(
                getPosition().x+m_sprite.getTexture()->getSize().x,
                getPosition().y);
	setLabel(label);
}

void Checkbox::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_sprite, state);
    target.draw(m_label, state);

    util::debugDrawFloatRect(target,state,getAllocation(),sf::Color::Blue);
}

void Checkbox::onMouseLeftDown(float x, float y)
{
	m_checked = !m_checked;
	updateSprite();
}

void Checkbox::onPositionChange()
{
    const sf::Vector2f & pos = getPosition();
	m_sprite.setPosition(pos);
    m_label.setPosition(pos.x+m_sprite.getTexture()->getSize().x,pos.y);
}

Checkbox::operator bool()
{
	return m_checked;
}

void Checkbox::updateSprite()
{
	if(m_checked)
		m_sprite.setTexture(TextureMgr::get("data/img/checkbox/checked.png"));
	else
		m_sprite.setTexture(TextureMgr::get("data/img/checkbox/unchecked.png"));
}

void Checkbox::setLabel(const std::string & label)
{
    m_label.setString(label);
    setAllocation(util::addFloatRects(m_sprite.getGlobalBounds(),m_label.getAllocation()));
}
