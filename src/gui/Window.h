//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef WINDOW_H
#define WINDOW_H

#include "Control.h"
#include "Message.h"
#include "TextureManager.h"

#include <iostream>
using namespace std;

class Window
	: public Control
	, public MessageHandler
{
public:
	Window(Control * parent)
		: Control(parent, false)
	{
		m_rect.setFillColor(sf::Color(10,14,18,180));
		m_rect.setOutlineColor(sf::Color(10,14,18,60));
		m_rect.setOutlineThickness(1);

		m_title.setFont(FontMgr::BebasNeue);
		m_title.setCharacterSize(24);
		m_title.setColor(sf::Color(236, 240, 241));

		Button * b = new Button();
		b->loadFromFile("data/img/close.png");
		b->setPosition(1.0f,0.0f);
		b->setOffset(-b->getSize().x - 5.0f, 5.0f);
		b->setColour(sf::Color(230, 126, 34));
		b->setHoverColour(sf::Color::White);
		b->setDownColour(sf::Color(211, 84, 0));
		b->setParent(this);
		b->setHandler(this, Message("hide"));
	}

	void setTitle(const std::string & title)
	{
		m_title.setString(title);
		sf::FloatRect r = m_title.getLocalBounds();
		// Truncate origin coordinates to integer values
		// so text is rendered crisp.
		m_title.setOrigin((int)(r.left+r.width/2) , 0);
	}

	void handleMessage(Message & message)
	{
		if(message.title == "hide")
			disable();
		if(message.title == "show")
			enable();
		if(message.title == "toggle")
		{
			if(m_enabled) disable();
			else enable();
		}
	}

	void drawSelf(sf::RenderTarget & target) const
	{
	    // Draw window's own elements
	    // (panel, title, close button, ...)
	    target.draw(m_rect);
		target.draw(m_title);

	    // Setup its view so elements overflowing its
	    // boundaries will be cropped. Remember that the
	    // old view is backed up by Control's draw() and will
	    // be restored after children are drawn.
	    sf::Vector2u tsize = target.getSize();

        sf::View view = target.getView();

        view.reset(sf::FloatRect(m_absolutePosition,m_size));
        view.setViewport(sf::FloatRect(
                m_absolutePosition.x / tsize.x,
                m_absolutePosition.y / tsize.y,
                m_size.x / tsize.x,
                m_size.y / tsize.y
        ));
        target.setView(view);
	}

	void onAbsolutePositionChanged()
	{
		m_rect.setPosition(m_absolutePosition);
		m_title.setPosition(m_absolutePosition.x+m_size.x/2.0f, m_absolutePosition.y);
	}

	void onChildSizeChanged(Control * child)
	{
		adaptToChild(child);
	}

	void onSizeChanged()
	{
		setOffset(-m_size/2.0f);
		m_rect.setSize(m_size);
	}

protected:
private:

	sf::RectangleShape m_rect;

	sf::Text m_title;

	void adaptToChild(Control * child)
	{
		sf::Vector2f diff = (child->getAbsolutePosition() + child->getSize())
							- (m_absolutePosition + m_size);

		sf::Vector2f newsize = m_size;
		if(diff.x > 0)
			newsize.x += diff.x;
		if(diff.y > 0)
			newsize.y += diff.y;

		setSize(newsize);
	}

};


#endif // WINDOW_H

