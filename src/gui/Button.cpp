//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Button.h"

#include "../ColourManager.h"
#include "../FontManager.h"
#include <iostream>

using namespace std;

Button::Button()
{
	m_rect.setFillColor(ColourMgr::Button);

	m_label.setFont(FontMgr::ButtonLabel);
	m_label.setCharacterSize(18);
}

void Button::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_rect, state);
    target.draw(m_label, state);
}

void Button::setLabel(const std::string & label)
{
	m_label.setString(label);
	m_label.setPosition(getPosition().x+5.0f,getPosition().y);
    sf::FloatRect bounds = m_label.getLocalBounds();
    sf::Vector2f size(bounds.width+bounds.left+10, bounds.height+2*bounds.top);
	m_rect.setSize(size);

	setSize(size);
}

void Button::onMouseEnter()
{
	m_rect.setFillColor(ColourMgr::ButtonHovered);
}

void Button::onMouseLeave()
{
	m_rect.setFillColor(ColourMgr::Button);
}

void Button::onMouseLeftUp(float x, float y)
{
    OnClick(*this);
}

void Button::onPositionChange()
{
	m_rect.setPosition(getPosition());
	m_label.setPosition(getPosition().x+5, getPosition().y);
}
