//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Label.h"

#include "../FontManager.h"
#include "../ColourManager.h"
#include "../Util.h"

Label::Label(const std::string & str)
{
	m_text.setFont(FontMgr::Label);
	m_text.setCharacterSize(14);
	m_text.setColor(ColourMgr::Label);
	setString(str);
}

void Label::setString(const std::string & str)
{
	m_text.setString(str);
    updateAllocation();
}

void Label::setFont(const sf::Font & font)
{
	m_text.setFont(font);
	updateAllocation();
}

void Label::setCharacterSize(unsigned int size)
{
	m_text.setCharacterSize(size);
	updateAllocation();
}

void Label::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_text, state);
    util::debugDrawFloatRect(target,state,getAllocation(),sf::Color::Green);
}

void Label::onPositionChange()
{
	m_text.setPosition(getPosition());
    updateAllocation();
}

void Label::updateAllocation()
{
    setAllocation(m_text.getGlobalBounds());
}
