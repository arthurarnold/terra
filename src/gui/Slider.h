//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef SLIDER_H
#define SLIDER_H

#include "Control.h"
#include "Label.h"
#include "../Animation.h"

class Slider : public Control
{
public:
	Slider();
	~Slider();

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

	// A Slider may capture a mouse move event even
	// when cursor is outside its allocation.
	bool captures(float x, float y) override;

    void onPositionChange() override;
	void onMouseEnter() override;
	void onMouseLeave() override;
	void onMouseLeftDown(float x, float y) override;
	void onMouseLeftUp(float x, float y) override;
	void onMouseMove(float x, float y) override;

	void setLimits(float min, float max);
	void setLabel(const std::string & label);

	void setWidth(float w);

    void setValue(float value);
    float getValue();

	////////// SIGNALS /////////////////////////////////////////////////////////

    // Emitted when slider's cursor is released.
	Signal<float> OnChange;
    // Emitted when slider's cursor is moved.
    Signal<float> OnLiveChange;

protected:
private:

	float m_min, m_max, m_current;
	bool m_down = false;

    anim::Handle m_animationGrow;
    anim::Handle m_animationShrink;

    Label m_label;

	sf::RectangleShape m_leftRect, m_rightRect;
	sf::CircleShape m_pointer;

	void updatePositions();
	void setPointerFromAbsoluteCoordinates(float x, float y);
    void setPointerRadius(float r);
};


#endif // SLIDER_H

