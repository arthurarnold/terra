//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef CONTROL_H
#define CONTROL_H

#include "SFML/Graphics.hpp"
#include <list>
#include "../Signal.h"

class Control : public sf::Drawable
{
public:

	enum class State : short
	{
		NORMAL = 0,		// Normal
		ACTIVE,			// Active (e.g. pressed)
		HOVERED,		// Mouse over
		DISABLED		// Disabled
	};

	// Virtual default destructor
	virtual ~Control() = default;


	////////// DRAW METHOD /////////////////////////////////////////////////////

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;


	////////// GETTERS/SETTERS /////////////////////////////////////////////////

	const sf::FloatRect & getAllocation() const;

	sf::Vector2f getPosition() const;
	void setPosition(float x, float y);
	void setPosition(const sf::Vector2f & position);

    sf::Vector2f getSize() const;

	State getState() const;


	////////// EXTERNAL EVENT HANDLING /////////////////////////////////////////

    Signal<const Control &> OnSizeChanged;

	// Only to be overridden by containers.
	virtual bool handleEvent(const sf::Event & event);



protected:

	// Constructor
	Control();

	void setAllocation(const sf::FloatRect & allocation);
	// Used by derived classes to set the allocation,
	// affecting both position and size at once.

	void setSize(float w, float h);
	void setSize(const sf::Vector2f & size);
	// Used by derived classes to set the size only.


    ////////// INTERNAL EVENT HANDLING /////////////////////////////////////////

		virtual bool captures(float x, float y);
		bool captures(const sf::Vector2f & point);

		// Mouse events //////////////////////////
			virtual void onMouseMove(float x, float y);
			virtual void onMouseEnter();
			virtual void onMouseLeave();
			virtual void onMouseLeftDown(float x, float y);
			virtual void onMouseLeftUp(float x, float y);

		virtual void onStateChange(State oldState);

        virtual void onPositionChange();

	////////////////////////////////////////////////////////////////////////////


private:

	State m_state;
	// The control's state.

	sf::FloatRect m_allocation;
	// The control's position and size.

	static Control * s_hoveredControl;
};


#endif // CONTROL_H

