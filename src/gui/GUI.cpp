//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "GUI.h"

#include <cstring>
#include <iostream>
#include <functional>
#include "../FontManager.h"
#include "../PlayState.h"
#include "../world/SpeciesManager.h"

#include "Button.h"
#include "Checkbox.h"
#include "Radio.h"
#include "Slider.h"
#include "../Animation.h"

#include "../WindowManager.h"

using namespace std;

void digestionRateSliderFunc(float v)
{
    Species * main = SpeciesMgr::get("main");
    float d = fabs(main->digestionRate - v) * 1000.0f;
    anim::createSettings("digestionRate")
            .from(main->digestionRate)
            .to(v)
            .duration(d);
    anim::start(anim::create(&(main->digestionRate), "digestionRate"));
}

void reproductionIntervalSliderFunc(float v)
{
	Species * main = SpeciesMgr::get("main");
	float d = fabs(main->reproductionInterval - v) * 0.005f;
	anim::createSettings("reproductionInterval")
			.from(main->reproductionInterval)
			.to(v)
			.duration(d);
	anim::start(anim::create(&(main->reproductionInterval), "reproductionInterval"));
}

void reproductiveCautionSliderFunc(float v)
{
	Species * main = SpeciesMgr::get("main");
	float d = fabs(main->mu - v) * 20.0f;
	anim::createSettings("reproductiveCaution")
			.from(main->mu)
			.to(v)
			.duration(d);
	anim::start(anim::create(&(main->mu), "reproductiveCaution"));
}

void precocitySliderFunc(float v)
{
	Species * main = SpeciesMgr::get("main");
	v = util::map(v, 0.0f,1.0f,0.5f,0.1f);
	float d = fabs(main->rho_m - v) * 25.0f;

	anim::createSettings("precocity")
			.from(main->rho_m)
			.to(v)
			.duration(d);
	anim::start(anim::create(&(main->rho_m), "precocity"));

}

void popCounterFunc(Label * lbl, int p)
{
    char buf[64];
    sprintf(buf, "Population: %d", p);
    lbl->setString(buf);
}

void GUI::togglePanel(Button & btn)
{
    using namespace std::placeholders;
    float y = m_sidePanel->getPosition().y;

    if(m_sidePanelVisible)
	{
		m_sidePanelVisible = false;
		btn.setLabel("Show panel");
        anim::start(anim::create(
                        std::bind((void (Control::*)(float,float))&Control::setPosition,m_sidePanel,_1,y),
                        "panelOut"));
	}
    else
	{
		m_sidePanelVisible = true;
		btn.setLabel("Hide panel");
        anim::start(anim::create(
                        std::bind((void (Control::*)(float,float))&Control::setPosition,m_sidePanel,_1,y),
                        "panelIn"));
	}
}


////////////////////////////////////////////////////////////////////////////////


void GUI::init()
{
	m_view.setSize(WindowMgr::getSize());
	m_view.setCenter(WindowMgr::getWidth()/2.0f, WindowMgr::getHeight()/2.0f);

	m_currentTool = NULL;
    m_defaultCursor.setTexture(TextureMgr::get("data/img/cursor.png"));
    m_cursor = &m_defaultCursor;

    m_sidePanel = new Container<>();
    m_sidePanel->setPosition(0.0f,50.0f);

	Button * button = new Button();
    button->setLabel("Show panel");
    button->setPosition(5,5);
    button->OnClick.connect(std::bind(&GUI::togglePanel,this,placeholders::_1));
    m_container.add(button);

//	RadioGroup * radioGroup = new RadioGroup();
//	radioGroup->add(new Radio("The one radio button"));
//	radioGroup->add(new Radio("The other radio button"));
//	radioGroup->add(new Radio("Yet another radio button"));
//	radioGroup->setPosition(5,100);
//    m_sidePanel->add(radioGroup);

    Species * species = SpeciesMgr::get("main");

	Slider * slider = new Slider();
    slider->setLimits(0.005f,0.015f);
    slider->setValue(species->digestionRate);
	slider->setWidth(100);
	slider->setLabel("Digestion rate");
	slider->setPosition(5,100);
    slider->OnChange.connect(digestionRateSliderFunc);
    m_sidePanel->add(slider);

    slider = new Slider();
    slider->setLimits(360.0f,2160.0f);
    slider->setValue(species->reproductionInterval);
	slider->setWidth(100);
	slider->setLabel("Reproduction\ninterval");
	slider->setPosition(5,200);
    slider->OnChange.connect(reproductionIntervalSliderFunc);
    m_sidePanel->add(slider);

    slider = new Slider();
    slider->setLimits(0.25f,1.0f);
    slider->setValue(species->mu);
	slider->setWidth(100);
	slider->setLabel("Reproductive\ncaution");
	slider->setPosition(5,300);
    slider->OnChange.connect(reproductiveCautionSliderFunc);
    m_sidePanel->add(slider);

    slider = new Slider();
    slider->setLimits(0.0f,1.0f);
    slider->setValue(util::map(species->rho_m,0.5f,0.1f));
	slider->setWidth(100);
	slider->setLabel("Precocity");
	slider->setPosition(5,400);
    slider->OnChange.connect(precocitySliderFunc);
    m_sidePanel->add(slider);


    Label * label = new Label();
    label->setFont(FontMgr::BebasNeue);
    label->setCharacterSize(18);
    popCounterFunc(label,species->getPopulation());
    label->setPosition(5,650);
    m_container.add(label);

    m_container.add(m_sidePanel);
    m_sidePanel->setPosition(-m_sidePanel->getSize().x,0.0f);

    SpeciesMgr::get("main")->OnPopulationChanged.connect(
                std::bind(popCounterFunc, label, std::placeholders::_1)
            );

    anim::createSettings("digestionRate")
            .interpolationFunction(anim::linear);

    anim::createSettings("panelOut")
            .duration(0.3f)
            .from(0.0f)
            .to(-m_sidePanel->getSize().x);
    anim::createSettings("panelIn")
            .duration(0.3f)
            .from(-m_sidePanel->getSize().x)
            .to(0.0f);
}

void GUI::draw(cgf::Game * game)
{
	sf::RenderWindow & window = *(game->getScreen());
	window.setView(m_view);
    sf::Vector2i pos = sf::Mouse::getPosition(window);

    // Draw controls
    window.draw(m_container);

	m_cursor->setPosition(pos.x, pos.y);
    window.draw(*m_cursor);
}

void GUI::update(cgf::Game * game)
{

}

void GUI::handleEvent(sf::Event & event)
{
	if(event.type == sf::Event::Resized)
	{
		m_view.setSize(event.size.width, event.size.height);
		m_view.setCenter(event.size.width/2.0f, event.size.height/2.0f);
	}
	else
		m_container.handleEvent(event);
}
