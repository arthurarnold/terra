//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef GUI_H
#define GUI_H

#include "Game.h"
#include "Tools.h"
#include "Button.h"

#include "Container.h"

class GUI
{
public:
	void init();

	void draw(cgf::Game * game);
	void update(cgf::Game * game);

	void handleEvent(sf::Event & event);


protected:
private:

	sf::View m_view;

    Container<> m_container;
    Container<> * m_sidePanel;
    bool m_sidePanelVisible = false;

	WaterTool m_waterTool;

    Tool * m_currentTool;

    sf::Sprite m_defaultCursor;
    sf::Sprite * m_cursor;

    void togglePanel(Button & btn);
    void initSidePanel();
};


#endif // GUI_H

