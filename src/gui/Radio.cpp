//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Radio.h"

#include "../TextureManager.h"

////////// RADIO CLASS /////////////////////////////////////////////////////////

Radio::Radio(const std::string & label, bool checked) :
	m_checked(checked)
{
	updateSprite();
	setSize((sf::Vector2f)m_sprite.getTexture()->getSize());
    m_label.setPosition(
                getPosition().x+m_sprite.getTexture()->getSize().x,
                getPosition().y);
	setLabel(label);
}

void Radio::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_sprite, state);
    target.draw(m_label, state);

    util::debugDrawFloatRect(target,state,getAllocation(),sf::Color::Blue);
}

void Radio::onMouseLeftDown(float x, float y)
{
	check();
}

void Radio::onPositionChange()
{
	const sf::Vector2f & pos = getPosition();
	m_sprite.setPosition(pos);
    m_label.setPosition(pos.x+m_sprite.getTexture()->getSize().x,pos.y);
}

bool Radio::checked()
{
	return m_checked;
}

void Radio::check()
{
	m_checked = true;
	updateSprite();
}

void Radio::uncheck()
{
	m_checked = false;
	updateSprite();
}

void Radio::setLabel(const std::string & label)
{
    m_label.setString(label);
    setAllocation(util::addFloatRects(m_sprite.getGlobalBounds(),m_label.getAllocation()));
}

Radio::operator bool()
{
	return m_checked;
}

void Radio::updateSprite()
{
	if(m_checked)
		m_sprite.setTexture(TextureMgr::get("data/img/radio/checked.png"));
	else
		m_sprite.setTexture(TextureMgr::get("data/img/radio/unchecked.png"));
}



////////// RADIO GROUP CLASS ///////////////////////////////////////////////////

const float RADIO_MARGIN = 5.0f;

#include <iostream>
using namespace std;

RadioGroup::RadioGroup() :
	m_selected(NULL),
	m_largestY(0)
{

}

void RadioGroup::add(Radio * r)
{
    r->setPosition(getPosition().x,m_largestY);
	m_largestY += r->getAllocation().height + RADIO_MARGIN;
    Container::add(r);
}

bool RadioGroup::handleEvent(const sf::Event & event)
{
//    sf::Event localEvent = util::makeLocalEvent(event,getPosition());
    return Control::handleEvent(event);
}

void RadioGroup::onMouseLeftDown(float x, float y)
{
    x -= getPosition().x;
    y -= getPosition().y;
	for(Radio * r : m_children)
		if(r->getAllocation().contains(x,y))
		{
			if(r != m_selected)
			{
				r->check();
				if(m_selected != NULL)
					m_selected->uncheck();
				m_selected = r;
			}
			return;
		}
}

void RadioGroup::onPositionChange()
{
    Container::onPositionChange();
}

Radio * RadioGroup::getSelected()
{
	return m_selected;
}
