//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef LABEL_H
#define LABEL_H

#include "Control.h"

class Label : public Control
{
public:
	Label(const std::string & str = "");
	~Label() = default;

	void setString(const std::string & str);
	void setFont(const sf::Font & font);
	void setCharacterSize(unsigned int size);

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

    void onPositionChange() override;

protected:
private:

	sf::Text m_text;

    void updateAllocation();

};


#endif // LABEL_H

