//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef CONTAINER_H
#define CONTAINER_H

#include "Control.h"
#include "../Util.h"

#include <iostream>
using namespace std;

template<typename T = Control>
class Container : public Control
{
public:

	Container()
	{
		m_background.setFillColor(sf::Color::Transparent);
	}

	virtual ~Container()
	{
		for(T * child : m_children)
			delete child;
	}

	// Add control to container.
	virtual void add(T * child)
	{
		m_children.push_back(child);
        child->OnSizeChanged.connect(std::bind(
                                         &Container::childSizeChanged,
                                         this,
                                         std::placeholders::_1));
        childSizeChanged(*child);
	}

	// Override from Control, but allow derived containers
	// to reimplement this method as suits them best.
	virtual void draw(sf::RenderTarget & target, sf::RenderStates state) const override
    {
        util::debugDrawFloatRect(target,state,getAllocation());
        state.transform.translate(getPosition());
        target.draw(m_background,state);
		for(T * child : m_children)
            target.draw(*child, state);

	}

	// These basic containers only distribute events to their
	// children without condition. Thus, these containers
	// occupy the entirety of their children's area.
	virtual bool handleEvent(const sf::Event & event) override
	{
        sf::Event localEvent = util::makeLocalEvent(event,getPosition());

		for(T * child : m_children)
            if(child->handleEvent(localEvent))
				return true;

		return false;
    }

    void childSizeChanged(const Control & child)
    {
        setAllocation(util::addFloatRects(getAllocation(),child.getAllocation(),true));
        m_background.setSize(getSize());
    }

    void setBackground(const sf::Color & color)
    {
    	m_background.setFillColor(color);
    }


protected:

	std::list<T *> m_children;
	sf::RectangleShape m_background;
};


#endif // CONTAINER_H

