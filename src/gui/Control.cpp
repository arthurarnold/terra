//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Control.h"

////////// STATIC CLASS MEMBERS ////////////////////////////////////////////////

Control * Control::s_hoveredControl = NULL;


////////// PROTECTED CONSTRUCTOR ///////////////////////////////////////////////

Control::Control() :
	m_state(State::NORMAL)
{
	// Control is created without parent,
	// so it is a root control.
}


////////// PUBLIC METHODS //////////////////////////////////////////////////////

void Control::draw(sf::RenderTarget & target, sf::RenderStates state) const
{

}

const sf::FloatRect & Control::getAllocation() const
{
	return m_allocation;
}

sf::Vector2f Control::getPosition() const
{
	return sf::Vector2f(m_allocation.left, m_allocation.top);
}

void Control::setPosition(float x, float y)
{
	m_allocation.left = x;
	m_allocation.top = y;
    onPositionChange();
}

void Control::setPosition(const sf::Vector2f & position)
{
	setPosition(position.x, position.y);
}

sf::Vector2f Control::getSize() const
{
	return sf::Vector2f(m_allocation.width, m_allocation.height);
}

Control::State Control::getState() const
{
    return m_state;
}

bool Control::handleEvent(const sf::Event & event)
{
	bool captured = false;
	if(m_state == State::DISABLED) return false;

	switch(event.type)
	{
		// MOUSE MOVE ////////////////////////////
		case sf::Event::MouseMoved:
			if(captures(event.mouseMove.x, event.mouseMove.y))
			{
				// Mouse is captured by control
				if(m_state == State::NORMAL)
				{
					// Mouse just entered control
					m_state = State::HOVERED;
					onStateChange(State::NORMAL);
					onMouseEnter();
					s_hoveredControl = this;
				}
				onMouseMove(event.mouseMove.x, event.mouseMove.y);
				captured = true;
			}
			else // Mouse not captured by control
			{
				if(m_state != State::NORMAL)
				{
					// Mouse just left control
					onMouseLeave();
					State oldState = m_state;
					m_state = State::NORMAL;
					onStateChange(oldState);
				}
			}
			break;

		// MOUSE BUTTON PRESSED //////////////////
		case sf::Event::MouseButtonPressed:
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				if(m_state == State::HOVERED)
				{
					m_state = State::ACTIVE;
					onStateChange(State::HOVERED);
					onMouseLeftDown(event.mouseButton.x, event.mouseButton.y);
					captured = true;
				}
			}
			break;

		// MOUSE BUTTON RELEASED /////////////////
		case sf::Event::MouseButtonReleased:
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				if(m_state == State::ACTIVE)
				{
					m_state = State::HOVERED;
					onStateChange(State::ACTIVE);
					onMouseLeftUp(event.mouseButton.x, event.mouseButton.y);
					captured = true;
				}
			}
			break;

		default:
			break;
	}

	return captured;
}


////////// PROTECTED METHODS ///////////////////////////////////////////////////

void Control::setAllocation(const sf::FloatRect & allocation)
{
	m_allocation = allocation;
    OnSizeChanged(*this);
}

void Control::setSize(float w, float h)
{
	m_allocation.width = w;
	m_allocation.height = h;
    OnSizeChanged(*this);
}

void Control::setSize(const sf::Vector2f & size)
{
	m_allocation.width = size.x;
	m_allocation.height = size.y;
    OnSizeChanged(*this);
}

bool Control::captures(float x, float y)
{
	return m_allocation.contains(x,y);
}

bool Control::captures(const sf::Vector2f & point)
{
	return captures(point.x, point.y);
}

void Control::onMouseMove(float x, float y)
{

}

void Control::onMouseEnter()
{

}

void Control::onMouseLeave()
{

}

void Control::onMouseLeftDown(float x, float y)
{

}

void Control::onMouseLeftUp(float x, float y)
{

}

void Control::onStateChange(State oldState)
{

}

void Control::onPositionChange()
{

}
