//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef BUTTON_H
#define BUTTON_H

#include "Control.h"
#include "../Signal.h"

class Button : public Control
{
public:
	Button();
	~Button() = default;

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

    void setLabel(const std::string & label);

	////////// SIGNALS /////////////////////////////////////////////////////////

	Signal<Button &> OnClick;

protected:
private:

	void onMouseEnter() override;
    void onMouseLeave() override;
    void onMouseLeftUp(float x, float y) override;

    void onPositionChange() override;

    sf::RectangleShape m_rect;

    sf::Text m_label;
};


#endif // BUTTON_H

