//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef CHECKBOX_H
#define CHECKBOX_H

#include "Control.h"
#include "Label.h"

class Checkbox : public Control
{
public:
	Checkbox(const std::string & label = "");
	~Checkbox() = default;

	void draw(sf::RenderTarget & target, sf::RenderStates state) const override;

	void onMouseLeftDown(float x, float y) override;
    void onPositionChange() override;

	operator bool();

	void setLabel(const std::string & label);


protected:
private:

	bool m_checked;

	sf::Sprite m_sprite;

	void updateSprite();

    Label m_label;

};


#endif // CHECKBOX_H

