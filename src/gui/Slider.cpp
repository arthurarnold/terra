//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#include "Slider.h"

#include "../ColourManager.h"
#include "../FontManager.h"
#include "../Util.h"
#include <iostream>
using namespace std;

const float RECT_THICKNESS = 2.0f;
const float POINTER_RADIUS = 6.0f;
const float POINTER_RADIUS_INC = 2.0f;

Slider::Slider()
{
	m_leftRect.setFillColor(ColourMgr::SliderOn);
	m_leftRect.setOrigin(0.0f,RECT_THICKNESS/2.0f);

	m_rightRect.setFillColor(ColourMgr::SliderOff);
	m_rightRect.setOrigin(0.0f,RECT_THICKNESS/2.0f);

	m_pointer.setRadius(POINTER_RADIUS);
	m_pointer.setFillColor(ColourMgr::SliderOn);
    m_pointer.setOutlineColor(ColourMgr::SliderOn);
	m_pointer.setOrigin(POINTER_RADIUS,POINTER_RADIUS);

    using namespace std::placeholders;

    if(!anim::hasSettings("sliderPointerGrow"))
    {
        anim::createSettings("sliderPointerGrow")
                .duration(0.2f)
                .from(POINTER_RADIUS)
                .to(POINTER_RADIUS + POINTER_RADIUS_INC);
    }
    m_animationGrow = anim::create(
                std::bind(&Slider::setPointerRadius,this,_1),
                "sliderPointerGrow");

    if(!anim::hasSettings("sliderPointerShrink"))
    {
        anim::createSettings("sliderPointerShrink")
                .duration(0.2f)
                .from(POINTER_RADIUS + POINTER_RADIUS_INC)
                .to(POINTER_RADIUS);
    }
    m_animationShrink = anim::create(
                std::bind(&Slider::setPointerRadius,this,_1),
                "sliderPointerShrink");
}

Slider::~Slider()
{

}

void Slider::draw(sf::RenderTarget & target, sf::RenderStates state) const
{
    target.draw(m_leftRect, state);
    target.draw(m_rightRect, state);
    target.draw(m_pointer, state);
    target.draw(m_label, state);
	//target.draw(m_text);

    util::debugDrawFloatRect(target,state,getAllocation());
}

////////// INTERNAL EVENTS /////////////////////////////////////////////////////

bool Slider::captures(float x, float y)
{
	if(m_down)
		return true;
	return getAllocation().contains(x,y);
}

void Slider::onPositionChange()
{
	updatePositions();
	sf::Vector2f pos = getPosition();
    pos.x += POINTER_RADIUS;
    pos.y = pos.y - m_label.getSize().y - 5.0f;
    m_label.setPosition(pos);
}

void Slider::onMouseEnter()
{
    anim::start(m_animationGrow);
}

void Slider::onMouseLeave()
{
    anim::start(m_animationShrink);
//	m_pointer.setRadius(POINTER_RADIUS);
//	m_pointer.setOrigin(POINTER_RADIUS,POINTER_RADIUS);

	m_down = false;
}

void Slider::onMouseLeftDown(float x, float y)
{
	float pointerRadius = m_pointer.getRadius();
	if(util::sqdist(m_pointer.getPosition(),sf::Vector2f(x,y)) > pointerRadius*pointerRadius)
	{
		// Cursor outside pointer. Move pointer to
		// cursor's position.
		setPointerFromAbsoluteCoordinates(x,y);
		updatePositions();
	}
		m_down = true;
}

void Slider::onMouseLeftUp(float x, float y)
{
	m_down = false;
	if(!getAllocation().contains(x,y))
	{
        anim::start(m_animationShrink);
    }
    OnChange(getValue());

}

void Slider::onMouseMove(float x, float y)
{
	if(m_down)
	{
		setPointerFromAbsoluteCoordinates(x,y);
		updatePositions();
	}
}

////////////////////////////////////////////////////////////////////////////////

void Slider::setLimits(float min, float max)
{
    m_min = min;
    m_max = max;
    m_current = (min + max) / 2.0f;
    updatePositions();
}

void Slider::setLabel(const std::string & label)
{
    m_label.setString(label);
    sf::Vector2f pos = getPosition();
    pos.x += POINTER_RADIUS;
    pos.y = pos.y - m_label.getSize().y - 5.0f;
    m_label.setPosition(pos);
}

void Slider::setWidth(float w)
{
	setSize(w+2.0f*POINTER_RADIUS,2*POINTER_RADIUS);
	updatePositions();
}

void Slider::updatePositions()
{
    sf::Vector2f pos = getPosition();
    pos.x += POINTER_RADIUS;
    pos.y += POINTER_RADIUS;

    float totalRectWidth = getSize().x - 2.0f*POINTER_RADIUS;

    m_leftRect.setSize(sf::Vector2f(m_current * totalRectWidth, RECT_THICKNESS));
	m_leftRect.setPosition(pos.x, pos.y);

    m_rightRect.setSize(sf::Vector2f((1.0f-m_current) * totalRectWidth, RECT_THICKNESS));
    m_rightRect.setPosition(pos.x + m_current*totalRectWidth, pos.y);


    m_pointer.setPosition(pos.x+m_current*totalRectWidth, pos.y);
}

void Slider::setPointerFromAbsoluteCoordinates(float x, float y)
{
	float oldValue = m_current;
	m_current = (x - getPosition().x) / getSize().x;
    m_current = std::max(m_current,0.0f);
    m_current = std::min(m_current,1.0f);

    if(m_current == 0.0f)
    {
        m_pointer.setOutlineThickness(-2);
        m_pointer.setFillColor(sf::Color::Transparent);
    }
    else
    {
        m_pointer.setOutlineThickness(0);
        m_pointer.setFillColor(ColourMgr::SliderOn);
    }

    if(m_current != oldValue)
        OnLiveChange(getValue());
}

void Slider::setValue(float value)
{
    value = std::max(value,m_min);
    value = std::min(value,m_max);

    m_current = util::map(value,m_min,m_max,0.0f,1.0f);
    updatePositions();
}

float Slider::getValue()
{
    return util::map(m_current, 0.0f, 1.0f, m_min, m_max);
}

void Slider::setPointerRadius(float r)
{
    m_pointer.setRadius(r);
    m_pointer.setOrigin(r,r);
}
