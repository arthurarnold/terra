//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef TOOLS_H
#define TOOLS_H

#include "../FontManager.h"
#include "../TextureManager.h"
#include "../ColourManager.h"

// TODO: fertility and pheromone tools.

class Tool
{
public:
	Tool() : m_isActive(false) {}
	virtual ~Tool() {}

	void setCursorTexture(sf::Texture & texture)
	{
		m_cursor.setTexture(texture);
	}

	void setCursorTexture(const std::string & texname)
	{
		m_cursor.setTexture(TextureMgr::get(texname));
	}

    sf::Sprite * getCursor() { return &m_cursor; }

    void activate() { m_isActive = true; }
    void deactivate() { m_isActive = false; }

	////////// EVENT HANDLING ////////////////////

	virtual void onMouseMove(float x, float y) = 0;
	virtual void onMouseDown(sf::Mouse::Button button) = 0;
	virtual void onMouseUp(sf::Mouse::Button button) = 0;

protected:
	sf::Sprite m_cursor;
	bool m_isActive;
};


////////// WATER TOOL //////////////////////////////////////////////////////////

#include "../world/World.h"
#include "../AudioManager.h"

class WaterTool : public Tool
{
public:
	WaterTool()
	{
	}

	~WaterTool() {}

	void onMouseMove(float x, float y)
	{
	}

	void onMouseDown(sf::Mouse::Button button)
	{
	}

	void onMouseUp(sf::Mouse::Button button)
	{
	}

private:

	sf::Vector2f m_mousePosition;
};


#endif // TOOLS_H

