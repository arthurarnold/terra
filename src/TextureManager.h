//
// Author: ARTHUR PAIM ARNOLD
// 2015
//

#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <string>
#include "Game.h"

namespace TextureMgr
{
	sf::Texture & get(const std::string & name);

	void cleanup();
}

#endif // TEXTURE_MANAGER_H

